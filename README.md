# last-fm
## Motivation
### Develop a music management app for android based on the LastFM API.
- With help of the app the user should be able to search for artists and pick one of the top albums which can be locally saved.
- The app should contain the following screens:
    - Main Screen: All locally stored albums are shown here. A tap on one of these albums opens a detail-page. With help of an entry in the menu it’s possible to go to the search screen.
    - Search Page: On this page it should be possible to search for an artist on the LastFM Api. Furthermore there should be a button next to a text field which starts the search. The results of the search should be shown in a list. A selection of one list-item opens the albums-overview screen.
    - Top-Alben: The best albums of an artist are shown here. It should be possible to store (and delete stored) albums locally. Tap on an album opens the detail-page. At least the following informations should be stored:
        - Name
        - Artist
        - Image (doesn't matter in which size)
        - Tracks
        - Detail page: Tap on an album opens the detail-page

### Create a fully tested Android application using best practices
- Clean Code.
- Dependency Injection.
- Clean Architecture.
- Test Driven Development.

## Build
To build this project you should have setup the following environment variables on your computer:

### ENV VARS
LAST_FM_HOCKEY_APP_ID
This you can get creating and application on [https://hockeyapp.net/](https://hockeyapp.net/).

LAST_FM_END_POINT
It is likely to be the same value for normal builds [http://ws.audioscrobbler.com/2.0/](http://ws.audioscrobbler.com/2.0/), if you have an endpoint for QA purposes this is the point to customize.

LAST_FM_API_KEY
To get your api key you should follow the instructions here [https://www.last.fm/api/account/create](https://www.last.fm/api/account/create).

Finally to build the project you should run the following command on the root of the project.

```gradle
gradlew clean assemble
```

## Test
To run unit tests execute the following command on the root of the project.
```gradle
gradlew clean test
```

## UI Test
To run UI tests execute the following command on the root of the project.
```gradle
gradlew clean connectedAndroidTest
```

## TODO
- Improve UI and UX
- Performance improvements
- Setup local server for integration tests on developer machine.

## Credits
This project heavily relies on concepts from:
- [Clean Code: A Handbook of Agile Software Craftsmanship](https://www.amazon.com/Clean-Code-Handbook-Software-Craftsmanship/dp/0132350882): Robert C. Martin book about writing good and readable code.
- [Ruby Midwest 2011 - Keynote: Architecture the Lost Years by Robert Martin](https://www.youtube.com/watch?v=WpkDN78P884): Robert C. Martin talk about Clean Architecture.
- [Test Driven Development: By Example](https://www.amazon.com/Test-Driven-Development-Kent-Beck/dp/0321146530): Kent Beck book about how to develop software guided by tests.