package com.ceduliocezar.lastfm.presentation;

import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.ceduliocezar.lastfm.R;
import com.ceduliocezar.lastfm.presentation.artist.search.ArtistSearchActivity;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.withId;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    @Rule
    public IntentsTestRule<MainActivity> activityRule = new IntentsTestRule<>(
            MainActivity.class,
            true,
            true);

    @Test
    public void test_clickSearch_it_should_show_artist_search_screen() {
        onView(withId(R.id.artist_search)).perform(click());
        intended(hasComponent(ArtistSearchActivity.class.getName()));
    }
}