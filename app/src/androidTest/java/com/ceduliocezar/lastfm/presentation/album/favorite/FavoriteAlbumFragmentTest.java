package com.ceduliocezar.lastfm.presentation.album.favorite;

import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.ceduliocezar.lastfm.R;
import com.ceduliocezar.lastfm.presentation.MainActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.Espresso.pressBack;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;


@RunWith(AndroidJUnit4.class)
public class FavoriteAlbumFragmentTest {

    @Rule
    public IntentsTestRule<MainActivity> activityRule = new IntentsTestRule<>(
            MainActivity.class,
            true,
            true);

    @Before
    public void setUp() {
        CountingIdlingResource mainActivityIdlingResource = activityRule.getActivity().countingIdlingResource;
        IdlingRegistry.getInstance().register(mainActivityIdlingResource);
    }

    @Test
    public void test_favorite_album() {
        onView(withId(R.id.artist_search)).perform(click());
        onView(withId(android.support.design.R.id.search_src_text)).perform(typeText("Cher"));
        onView(withId(R.id.artist_recycler)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        onView(withId(R.id.top_album_recycler)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));
        onView(withId(R.id.favorite_button)).perform(click());
        pressBack();
        pressBack();
        pressBack();
        pressBack();
    }
}