package com.ceduliocezar.lastfm.presentation.album.top;

import android.content.Intent;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.ceduliocezar.lastfm.model.entity.Artist;
import com.ceduliocezar.lastfm.presentation.album.detail.AlbumDetailActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.ceduliocezar.lastfm.presentation.Matchers.first;


@RunWith(AndroidJUnit4.class)
public class TopAlbumActivityTest {

    @Rule
    public IntentsTestRule<TopAlbumActivity> activityRule = new IntentsTestRule<>(
            TopAlbumActivity.class,
            true,
            false);

    @Test
    public void test_show_top_album() {
        Intent intent = new Intent();
        intent.putExtra(TopAlbumActivity.EXTRA_ARTIST, new Artist("bfcc6d75-a6a5-4bc6-8282-47aec8531818", "Cher"));

        activityRule.launchActivity(intent);

        CountingIdlingResource mainActivityIdlingResource = activityRule.getActivity().countingIdlingResource;
        IdlingRegistry.getInstance().register(mainActivityIdlingResource);

        onView(first(withText("Believe"))).check(matches(isDisplayed()));
        onView(withText("Cher")).check(matches(isDisplayed()));
        onView(first(withText("Believe"))).perform(click());
        intended(hasComponent(AlbumDetailActivity.class.getName()));
    }

}