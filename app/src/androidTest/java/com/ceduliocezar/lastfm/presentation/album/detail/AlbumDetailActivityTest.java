package com.ceduliocezar.lastfm.presentation.album.detail;

import android.content.Intent;
import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.ceduliocezar.lastfm.R;
import com.ceduliocezar.lastfm.model.entity.Album;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static com.ceduliocezar.lastfm.presentation.Matchers.first;


@RunWith(AndroidJUnit4.class)
public class AlbumDetailActivityTest {

    @Rule
    public IntentsTestRule<AlbumDetailActivity> activityRule = new IntentsTestRule<>(
            AlbumDetailActivity.class,
            true,
            false);

    @Before
    public void setUp() {

    }

    @Test
    public void test_search_show_album_detail() {


        Intent intent = new Intent();
        intent.putExtra(AlbumDetailActivity.EXTRA_ALBUM, new Album("63b3a8ca-26f2-4e2b-b867-647a6ec2bebd", "Believe", "bfcc6d75-a6a5-4bc6-8282-47aec8531818"));

        activityRule.launchActivity(intent);

        CountingIdlingResource mainActivityIdlingResource = activityRule.getActivity().countingIdlingResource;
        IdlingRegistry.getInstance().register(mainActivityIdlingResource);

        onView(first(withText("Believe"))).check(matches(isDisplayed()));
        onView(withText("Cher")).check(matches(isDisplayed()));
        onView(withId(R.id.favorite_button)).perform(click());
        onView(withText(R.string.album_saved_as_favorite)).check(matches(isDisplayed()));
    }

}