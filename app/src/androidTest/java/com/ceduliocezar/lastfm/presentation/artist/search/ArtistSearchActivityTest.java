package com.ceduliocezar.lastfm.presentation.artist.search;

import android.support.test.espresso.IdlingRegistry;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.test.espresso.intent.rule.IntentsTestRule;
import android.support.test.runner.AndroidJUnit4;

import com.ceduliocezar.lastfm.R;
import com.ceduliocezar.lastfm.presentation.album.top.TopAlbumActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.intent.Intents.intended;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static android.support.test.espresso.intent.matcher.IntentMatchers.hasExtraWithKey;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.AllOf.allOf;

@RunWith(AndroidJUnit4.class)
public class ArtistSearchActivityTest {

    @Rule
    public IntentsTestRule<ArtistSearchActivity> activityRule = new IntentsTestRule<>(
            ArtistSearchActivity.class,
            true,
            true);

    @Before
    public void setUp() {
        CountingIdlingResource mainActivityIdlingResource = activityRule.getActivity().countingIdlingResource;
        IdlingRegistry.getInstance().register(mainActivityIdlingResource);
    }

    @Test
    public void test_search_artist_and_select_it_should_show_top_album() {
        onView(withId(R.id.action_search)).perform(click());
        onView(withId(android.support.design.R.id.search_src_text)).perform(typeText("Scorpions"));
        onView(withId(R.id.artist_recycler)).perform(RecyclerViewActions.actionOnItemAtPosition(0, click()));

        intended(allOf(hasComponent(TopAlbumActivity.class.getName()),
                hasExtraWithKey(TopAlbumActivity.EXTRA_ARTIST)));

        onView(withText("Scorpions")).check(matches(isDisplayed()));
    }
}