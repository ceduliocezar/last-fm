package com.ceduliocezar.lastfm.model.data.artist.local;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.data.AppDataBase;
import com.ceduliocezar.lastfm.model.entity.Artist;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import io.reactivex.Single;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Test suite for {@link LocalArtistDataSource}
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(AppLog.class)
public class LocalArtistDataSourceTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private LocalArtistDataSource subjectUnderTesting;

    @Mock
    private AppDataBase appDataBase;

    @Mock
    private Artist artist;

    @Mock
    private ArtistDAO artistDAO;

    @Mock
    private RuntimeException exception;

    @Test(expected = UnsupportedOperationException.class)
    public void test_list_it_should_fail() {
        subjectUnderTesting.list("search");
    }

    @Test
    public void test_save_it_should_complete() {
        mockDAO();

        subjectUnderTesting.save(artist)
                .test()
                .assertComplete();

        verify(artistDAO).insert(any());
    }

    @Test
    public void test_save_it_should_fail() {
        mockDAO();

        doThrow(exception).when(artistDAO).insert(any());
        subjectUnderTesting.save(artist)
                .test()
                .assertNotComplete()
                .assertError(error -> error == exception);
    }

    private void mockDAO() {
        when(appDataBase.artistDAO()).thenReturn(artistDAO);
    }

    @Test
    public void test_getArtist_it_should_complete() {
        mockDAO();
        when(artistDAO.get("id")).thenReturn(Single.just(new RoomArtist("id", "name")));
        subjectUnderTesting.getArtist("id")
                .test()
                .assertComplete()
                .assertValue(result -> result.getId().equals("id") && result.getName().equals("name"));
    }

    @Test
    public void test_getArtist_it_should_fail() {
        mockDAO();
        when(artistDAO.get("id")).thenReturn(Single.error(exception));
        subjectUnderTesting.getArtist("id")
                .test()
                .assertNotComplete()
                .assertError(error -> error == exception);
    }
}