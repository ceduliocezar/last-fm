package com.ceduliocezar.lastfm.model.interactor.album;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.repository.album.AlbumRepository;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import io.reactivex.Completable;
import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;


/**
 * Test suite for {@link SaveAlbum}
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(AppLog.class)
public class SaveAlbumTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private SaveAlbum subjectUnderTesting;

    @Mock
    private AlbumRepository albumRepository;

    @Mock
    private Throwable exception;

    @Mock
    private Album album;

    @Test
    public void test_buildSingle_it_should_complete() {

        when(albumRepository.save(eq(album))).thenReturn(Completable.complete());

        TestObserver<Void> testObserver = subjectUnderTesting.buildCompletable(album).test();

        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
    }

    @Test
    public void test_buildSingle_it_should_fail_when_repository_fails() {

        when(albumRepository.save(eq(album))).thenReturn(Completable.error(exception));

        TestObserver<Void> testObserver = subjectUnderTesting.buildCompletable(album).test();

        testObserver.awaitTerminalEvent();
        testObserver.assertNotComplete();
        testObserver.assertError(error -> error == exception);
    }

}