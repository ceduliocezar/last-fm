package com.ceduliocezar.lastfm.model.data.image.local;

import android.content.Context;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.data.AppDataBase;
import com.ceduliocezar.lastfm.model.data.FileWrapper;
import com.ceduliocezar.lastfm.model.data.ImageLoader;
import com.ceduliocezar.lastfm.model.data.album.image.AlbumImageDAO;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Image;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.File;
import java.io.IOException;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

/**
 * Test suite for {@link LocalImageDataSource}
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(AppLog.class)
public class LocalImageDataSourceTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private LocalImageDataSource subjectUnderTesting;

    @Mock
    private AppDataBase appDataBase;

    @Mock
    private Context context;

    @Mock
    private FileWrapper fileWrapper;

    @Mock
    private ImageLoader imageLoader;

    @Mock
    private Image image;

    @Mock
    private AlbumImageDAO imageDAO;

    @Mock
    private RuntimeException exception;

    @Mock
    private File imageFile;

    @Mock
    private File filesDir;

    @Mock
    private Album album;

    @Before
    public void setUp() {
        mockStatic(AppLog.class);
    }

    @Test
    public void test_getImage_it_should_complete() {
        mockDAO();
        when(imageDAO.get("id")).thenReturn(Single.just(image));
        subjectUnderTesting.getImage("id")
                .test()
                .assertComplete()
                .assertValue(result -> result == image);
    }

    @Test
    public void test_getImage_it_should_fail() {
        mockDAO();
        when(imageDAO.get("id")).thenReturn(Single.error(exception));
        subjectUnderTesting.getImage("id")
                .test()
                .assertNotComplete()
                .assertError(error -> error == exception);
    }

    private void mockDAO() {
        when(appDataBase.albumImageDAO()).thenReturn(imageDAO);
    }

    @Test
    public void test_saveAlbumImage_it_should_complete() throws IOException {
        mockDAO();
        when(image.getId()).thenReturn("id");
        when(imageFile.getAbsolutePath()).thenReturn("//absolute/path");
        when(fileWrapper.newFileInstance(any(), any())).thenReturn(imageFile);
        when(context.getFilesDir()).thenReturn(filesDir);
        TestObserver<Void> testObserver = subjectUnderTesting.saveAlbumImage(image).test();
        testObserver.assertComplete();

        verify(imageFile).createNewFile();
        verify(imageLoader).downloadFile(any(), any());
        verify(imageDAO).insert(any());
    }

    @Test
    public void test_saveAlbumImage_it_should_fail() {
        doThrow(exception).when(fileWrapper).newFileInstance(any(), any());

        subjectUnderTesting.saveAlbumImage(image)
                .test()
                .assertNotComplete()
                .assertError(error -> error == exception);
    }

    @Test
    public void test_deleteImage_it_should_complete() {
        when(album.getId()).thenReturn("id");
        when(context.getFilesDir()).thenReturn(filesDir);
        when(fileWrapper.newFileInstance(any(), any())).thenReturn(imageFile);

        subjectUnderTesting.deleteImage(album)
                .test()
                .assertComplete();

        verify(imageFile).delete();
    }

    @Test
    public void test_deleteImage_it_should_fail() {
        doThrow(exception).when(fileWrapper).newFileInstance(any(), any());

        when(album.getId()).thenReturn("id");
        subjectUnderTesting.deleteImage(album)
                .test()
                .assertNotComplete()
                .assertError(error -> error == exception);
    }
}