package com.ceduliocezar.lastfm.model.data.artist;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.data.APIService;
import com.ceduliocezar.lastfm.model.data.artist.cloud.Artist;
import com.ceduliocezar.lastfm.model.data.artist.cloud.ArtistTO;
import com.ceduliocezar.lastfm.model.data.artist.cloud.CloudArtistDataSource;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

/**
 * Test suite for {@link CloudArtistDataSource}
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(AppLog.class)
public class CloudArtistDataSourceTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private CloudArtistDataSource subjectUnderTesting;

    @Mock
    private APIService service;

    @Mock
    private Artist artistEntity;

    @Mock
    private com.ceduliocezar.lastfm.model.entity.Artist artist;

    @Mock
    private ArtistTO artistTO;

    @Mock
    private Throwable exception;

    @Before
    public void setUp() {
        mockStatic(AppLog.class);
    }

    @Test
    public void test_list_it_should_return_same_list() {
        List<Artist> artists = new ArrayList<>();
        artists.add(artistEntity);
        when(artistEntity.getId()).thenReturn("id");
        when(artistEntity.getName()).thenReturn("name");

        when(service.artists(eq("scor"))).thenReturn(Single.just(artists));

        TestObserver<List<com.ceduliocezar.lastfm.model.entity.Artist>> testObserver = subjectUnderTesting.list("scor").test();
        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertValue(artists1 -> artists1.size() == 1 && artists1.get(0).getId().equals("id") && artists1.get(0).getName().equals("name"));
    }

    @Test(expected = UnsupportedOperationException.class)
    public void test_save_it_should_fail() {
        subjectUnderTesting.save(artist);
    }

    @Test
    public void test_getArtist_it_should_complete() {
        when(artistTO.getArtist()).thenReturn(artistEntity);
        when(service.artist("id")).thenReturn(Single.just(artistTO));
        subjectUnderTesting.getArtist("id")
                .test()
                .assertComplete()
                .assertValue(Objects::nonNull);
    }

    @Test
    public void test_getArtist_it_should_fail() {

        when(service.artist("id")).thenReturn(Single.error(exception));
        subjectUnderTesting.getArtist("id")
                .test()
                .assertNotComplete()
                .assertError(error -> error == exception);
    }
}