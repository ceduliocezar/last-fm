package com.ceduliocezar.lastfm.model.interactor.album;

import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.repository.album.AlbumRepository;
import com.ceduliocezar.lastfm.model.repository.image.ImageRepository;
import com.ceduliocezar.lastfm.model.repository.track.TrackRepository;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.modules.junit4.PowerMockRunner;

import io.reactivex.Completable;
import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * Test suite for {@link DeleteAlbum}
 */
@RunWith(PowerMockRunner.class)
public class DeleteAlbumTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private DeleteAlbum subjectUnderTesting;


    @Mock
    private AlbumRepository albumRepository;

    @Mock
    private Album album;

    @Mock
    private TrackRepository trackRepository;

    @Mock
    private ImageRepository imageRepository;

    @Mock
    private Throwable exception;


    @Test
    public void test_buildCompletable_it_should_complete() {
        when(albumRepository.delete(eq(album))).thenReturn(Completable.complete());
        when(trackRepository.deleteAlbumTracks(album)).thenReturn(Completable.complete());
        when(imageRepository.deleteAlbumImage(album)).thenReturn(Completable.complete());

        TestObserver<Void> testObserver = subjectUnderTesting.buildCompletable(album).test();

        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
    }

    @Test
    public void test_buildCompletable_if_album_repository_fail_it_should_fail() {
        when(albumRepository.delete(eq(album))).thenReturn(Completable.error(exception));
        when(trackRepository.deleteAlbumTracks(album)).thenReturn(Completable.complete());
        when(imageRepository.deleteAlbumImage(album)).thenReturn(Completable.complete());

        TestObserver<Void> testObserver = subjectUnderTesting.buildCompletable(album).test();

        testObserver.awaitTerminalEvent();
        testObserver.assertNotComplete();
    }

    @Test
    public void test_buildCompletable_if_track_repository_fail_it_should_fail() {
        when(albumRepository.delete(eq(album))).thenReturn(Completable.complete());
        when(trackRepository.deleteAlbumTracks(album)).thenReturn(Completable.error(exception));
        when(imageRepository.deleteAlbumImage(album)).thenReturn(Completable.complete());

        TestObserver<Void> testObserver = subjectUnderTesting.buildCompletable(album).test();

        testObserver.awaitTerminalEvent();
        testObserver.assertNotComplete();
    }

    @Test
    public void test_buildCompletable_if_image_repository_fail_it_should_fail() {
        when(albumRepository.delete(eq(album))).thenReturn(Completable.complete());
        when(trackRepository.deleteAlbumTracks(album)).thenReturn(Completable.complete());
        when(imageRepository.deleteAlbumImage(album)).thenReturn(Completable.error(exception));

        TestObserver<Void> testObserver = subjectUnderTesting.buildCompletable(album).test();

        testObserver.awaitTerminalEvent();
        testObserver.assertNotComplete();
    }

}