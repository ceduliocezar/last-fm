package com.ceduliocezar.lastfm.model.data.album;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.data.AppDataBase;
import com.ceduliocezar.lastfm.model.data.album.local.AlbumDAO;
import com.ceduliocezar.lastfm.model.data.album.local.LocalAlbumDataSource;
import com.ceduliocezar.lastfm.model.data.album.local.RoomAlbum;
import com.ceduliocezar.lastfm.model.entity.Album;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.doThrow;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

/**
 * Test suite for {@link LocalAlbumDataSource}
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(AppLog.class)
public class LocalAlbumDataSourceTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private LocalAlbumDataSource subjectUnderTesting;

    @Mock
    private Album album;

    @Mock
    private AppDataBase appDataBase;

    @Mock
    private AlbumDAO albumDAO;

    @Mock
    private RuntimeException exception;

    @Before
    public void setUp() {
        mockStatic(AppLog.class);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void test_topAlbums_it_should_always_fail_since_currently_local_top_albums_are_not_supported() {
        String artistId = "c3cceeed-3332-4cf0-8c4c-bbde425147b6";
        subjectUnderTesting.topAlbums(artistId);
    }

    @Test
    public void test_save_it_should_complete() {
        mockDAO();
        TestObserver<Void> testObserver = subjectUnderTesting.save(album).test();
        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();

        verify(albumDAO).insert(any());
    }

    private void mockDAO() {
        when(appDataBase.albumDAO()).thenReturn(albumDAO);
    }

    @Test
    public void test_favoriteAlbums_it_should_complete() {
        List<RoomAlbum> albumsRoom = new ArrayList<>();
        albumsRoom.add(new RoomAlbum("id", "name", "artistId"));
        mockDAO();
        when(albumDAO.favoriteAlbums()).thenReturn(Single.just(albumsRoom));
        subjectUnderTesting.favoriteAlbums()
                .test()
                .assertComplete()
                .assertValue(result -> result.size() == 1 && result.get(0).getName().equals("name"));
    }

    @Test
    public void test_favoriteAlbums_it_should_fail() {
        mockDAO();
        when(albumDAO.favoriteAlbums()).thenReturn(Single.error(exception));
        subjectUnderTesting.favoriteAlbums()
                .test()
                .assertNotComplete()
                .assertError(error -> error == exception);
    }

    @Test
    public void test_delete_it_should_complete() {
        mockDAO();
        subjectUnderTesting.delete(album)
                .test()
                .assertComplete();

        verify(albumDAO).delete(any());
    }

    @Test
    public void test_delete_it_should_fail() {
        mockDAO();
        doThrow(exception).when(albumDAO).delete(any());
        subjectUnderTesting.delete(album)
                .test()
                .assertNotComplete()
                .assertError(error -> error == exception);
    }

    @Test
    public void test_getAlbum_it_should_complete() {
        mockDAO();
        when(albumDAO.get("id")).thenReturn(Single.just(new RoomAlbum("id", "name", "artistId")));
        subjectUnderTesting.getAlbum("id")
                .test()
                .assertComplete()
                .assertValue(result -> result.getId().equals("id") && result.getName().equals("name"));
    }

    @Test
    public void test_getAlbum_it_should_fail() {
        mockDAO();
        when(albumDAO.get("id")).thenReturn(Single.error(exception));
        subjectUnderTesting.getAlbum("id")
                .test()
                .assertNotComplete()
                .assertError(error -> error == exception);
    }
}