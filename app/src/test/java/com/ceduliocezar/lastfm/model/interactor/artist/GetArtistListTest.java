package com.ceduliocezar.lastfm.model.interactor.artist;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.repository.artist.ArtistRepository;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

/**
 * Test suite for {@link GetArtistList}
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(AppLog.class)
public class GetArtistListTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private GetArtistList subjectUnderTesting;

    @Mock
    private ArtistRepository artistRepository;

    @Mock
    private com.ceduliocezar.lastfm.model.entity.Artist artist;

    @Mock
    private RuntimeException exception;

    @Before
    public void setUp() {
        mockStatic(AppLog.class);
    }

    @Test
    public void test_buildSingle_it_should_list_results_from_artist_repository() {
        final List<com.ceduliocezar.lastfm.model.entity.Artist> artistList = new ArrayList<>();
        artistList.add(artist);
        artistList.add(artist);
        when(artistRepository.list(eq("scorpions"))).thenReturn(Single.just(artistList));

        TestObserver<List<com.ceduliocezar.lastfm.model.entity.Artist>> testObserver = subjectUnderTesting.buildSingle("scorpions").test();
        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertValue(result -> result == artistList);
    }

    @Test
    public void test_buildSingle_it_should_dispatch_call_onError_when_an_exception_occurred() {
        when(artistRepository.list(eq("scorpions"))).thenReturn(Single.error(exception));
        TestObserver<List<com.ceduliocezar.lastfm.model.entity.Artist>> testObserver = subjectUnderTesting.buildSingle("scorpions").test();
        testObserver.awaitTerminalEvent();
        testObserver.assertNotComplete();
    }
}