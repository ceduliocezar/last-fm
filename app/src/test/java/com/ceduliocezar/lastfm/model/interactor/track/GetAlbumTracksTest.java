package com.ceduliocezar.lastfm.model.interactor.track;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.entity.Track;
import com.ceduliocezar.lastfm.model.repository.track.TrackRepository;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;


@RunWith(PowerMockRunner.class)
@PrepareForTest(AppLog.class)
public class GetAlbumTracksTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private GetAlbumTracks subjectUnderTesting;

    @Mock
    private TrackRepository trackRepository;

    @Mock
    private RuntimeException exception;

    @Before
    public void setUp() {
        mockStatic(AppLog.class);
    }

    @Test
    public void test_buildSingle_it_should_list_results_from_artist_repository() {
        final List<Track> tracks = new ArrayList<>();
        when(trackRepository.getAlbumTracks(eq("id"))).thenReturn(Single.just(tracks));

        TestObserver<List<Track>> testObserver = subjectUnderTesting.buildSingle("id").test();
        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertValue(result -> result == tracks);
    }

    @Test
    public void test_buildSingle_it_should_dispatch_call_onError_when_an_exception_occurred() {
        when(trackRepository.getAlbumTracks(eq("id"))).thenReturn(Single.error(exception));
        TestObserver<List<Track>> testObserver = subjectUnderTesting.buildSingle("id").test();
        testObserver.awaitTerminalEvent();
        testObserver.assertNotComplete();
    }
}