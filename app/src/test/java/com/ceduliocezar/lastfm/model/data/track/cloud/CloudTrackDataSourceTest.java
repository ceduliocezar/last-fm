package com.ceduliocezar.lastfm.model.data.track.cloud;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.data.APIService;
import com.ceduliocezar.lastfm.model.entity.Album;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;

import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest(AppLog.class)
public class CloudTrackDataSourceTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private CloudTrackDataSource subjectUnderTesting;

    @Mock
    private APIService apiService;

    @Mock
    private Album album;

    @Before
    public void setUp() {
        mockStatic(AppLog.class);
    }

    @Test
    public void test_list() {
        List<Track> tracks = new ArrayList<>();
        tracks.add(new Track("name"));
        when(apiService.tracks("id")).thenReturn(Single.just(tracks));

        subjectUnderTesting.list("id")
                .test()
                .assertComplete()
                .assertValue(result -> result.size() == 1);

    }

    @Test(expected = UnsupportedOperationException.class)
    public void test_save_it_should_fail_operation_not_available() {
        List<com.ceduliocezar.lastfm.model.entity.Track> tracks = new ArrayList<>();
        subjectUnderTesting.save(tracks, "id");
    }

    @Test(expected = UnsupportedOperationException.class)
    public void test_deleteTracks_it_should_fail_operation_not_supported() {

        subjectUnderTesting.deleteTracks(album);
    }
}