package com.ceduliocezar.lastfm.model.interactor.album;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.repository.album.AlbumRepository;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@RunWith(PowerMockRunner.class)
@PrepareForTest(AppLog.class)
public class VerifyFavoriteTest {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private VerifyFavorite subjectUnderTesting;

    @Mock
    private AlbumRepository albumRepository;

    @Mock
    private Throwable exception;

    @Mock
    private Album album;

    @Test
    public void test_buildSingle_it_should_return_true() {

        when(albumRepository.getAlbum(any())).thenReturn(Single.just(album));

        TestObserver<Boolean> testObserver = subjectUnderTesting.buildSingle("album_id").test();

        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertValue(result -> result);
    }

    @Test
    public void test_buildSingle_it_should_fail_when_repository_fails() {

        when(albumRepository.getAlbum(any())).thenReturn(Single.error(exception));

        TestObserver<Boolean> testObserver = subjectUnderTesting.buildSingle(null).test();

        testObserver.awaitTerminalEvent();
        testObserver.assertNotComplete();
        testObserver.assertError(error -> error == exception);
    }

}