package com.ceduliocezar.lastfm.model.repository.track;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.data.track.TrackDataSource;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Track;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest(AppLog.class)
public class DefaultTrackRepositoryTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    private DefaultTrackRepository subjectUnderTesting;

    @Mock
    private TrackDataSource localTrackDataSource;

    @Mock
    private TrackDataSource cloudTrackDataSource;

    @Mock
    private Track track;

    @Mock
    private Throwable exception;
    @Mock
    private Album album;

    @Before
    public void setUp() {
        mockStatic(AppLog.class);
        subjectUnderTesting = new DefaultTrackRepository(localTrackDataSource, cloudTrackDataSource);
    }

    @Test
    public void test_getAlbumTracks_it_should_return_local_data() {
        List<Track> tracks = new ArrayList<>();
        tracks.add(track);
        when(localTrackDataSource.list("id")).thenReturn(Single.just(tracks));
        TestObserver<List<Track>> testObserver = subjectUnderTesting.getAlbumTracks("id").test();
        testObserver.assertComplete();
        testObserver.assertValue(result -> result == tracks);
    }

    @Test
    public void test_getAlbumTracks_it_should_return_cloud_data_when_there_is_no_data_locally() {
        List<Track> tracks = new ArrayList<>();
        List<Track> cloudTracks = new ArrayList<>();
        cloudTracks.add(track);
        when(localTrackDataSource.list("id")).thenReturn(Single.just(tracks));
        when(cloudTrackDataSource.list("id")).thenReturn(Single.just(cloudTracks));

        TestObserver<List<Track>> testObserver = subjectUnderTesting.getAlbumTracks("id").test();

        testObserver.assertComplete();
        testObserver.assertValue(result -> result == cloudTracks);
    }

    @Test
    public void test_getAlbumTracks_it_should_fail_if_local_fails() {

        when(localTrackDataSource.list("id")).thenReturn(Single.error(exception));

        TestObserver<List<Track>> testObserver = subjectUnderTesting.getAlbumTracks("id").test();
        testObserver.assertNotComplete();
        testObserver.assertError(error -> error == exception);
    }

    @Test
    public void test_getAlbumTracks_it_should_fail_if_cloud_fails() {

        List<Track> tracks = new ArrayList<>();
        when(localTrackDataSource.list("id")).thenReturn(Single.just(tracks));
        when(cloudTrackDataSource.list("id")).thenReturn(Single.error(exception));
        TestObserver<List<Track>> testObserver = subjectUnderTesting.getAlbumTracks("id").test();
        testObserver.assertNotComplete();
        testObserver.assertError(error -> error == exception);
    }

    @Test
    public void test_saveAlbumTracks_it_should_complete() {

        List<Track> tracks = new ArrayList<>();
        when(localTrackDataSource.save(tracks, "id")).thenReturn(Completable.complete());
        TestObserver<Void> testObserver = subjectUnderTesting.saveAlbumTracks(tracks, "id").test();
        testObserver.assertComplete();
    }

    @Test
    public void test_saveAlbumTracks_it_should_fail_when_local_fail() {

        List<Track> tracks = new ArrayList<>();
        when(localTrackDataSource.save(tracks, "id")).thenReturn(Completable.error(exception));
        subjectUnderTesting.saveAlbumTracks(tracks, "id")
                .test()
                .assertNotComplete();
    }

    @Test
    public void test_deleteAlbumTracks_it_should_complete() {
        when(localTrackDataSource.deleteTracks(album)).thenReturn(Completable.complete());
        subjectUnderTesting.deleteAlbumTracks(album)
                .test()
                .assertComplete();
    }

    @Test
    public void test_deleteAlbumTracks_it_should_not_complete() {
        when(localTrackDataSource.deleteTracks(album)).thenReturn(Completable.error(exception));
        subjectUnderTesting.deleteAlbumTracks(album)
                .test()
                .assertNotComplete()
                .assertError(error -> error == exception);
    }
}