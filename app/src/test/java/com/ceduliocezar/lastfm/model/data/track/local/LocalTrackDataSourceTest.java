package com.ceduliocezar.lastfm.model.data.track.local;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.data.AppDataBase;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Track;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;

import static org.mockito.ArgumentMatchers.anyList;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest(AppLog.class)
public class LocalTrackDataSourceTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private LocalTrackDataSource subjectUnderTesting;

    @Mock
    private AppDataBase appDataBase;

    @Mock
    private TrackDAO trackDAO;

    @Mock
    private RoomTrack roomTrack;

    @Mock
    private RuntimeException exception;

    @Mock
    private Album album;

    @Before
    public void setUp() {
        mockStatic(AppLog.class);
    }

    @Test
    public void test_list_it_should_complete() {
        List<RoomTrack> tracks = new ArrayList<>();
        tracks.add(roomTrack);
        when(roomTrack.getName()).thenReturn("name");
        mockDAO();
        when(trackDAO.list("id")).thenReturn(Single.just(tracks));

        subjectUnderTesting.list("id")
                .test()
                .assertComplete()
                .assertValue(result -> result.size() == 1 && result.get(0).getName().equals("name"));
    }

    @Test
    public void test_list_it_should_complete_with_empty_list() {
        List<RoomTrack> tracks = new ArrayList<>();
        mockDAO();
        when(trackDAO.list("id")).thenReturn(Single.just(tracks));

        subjectUnderTesting.list("id")
                .test()
                .assertComplete()
                .assertValue(List::isEmpty);
    }

    @Test
    public void test_list_it_should_fail_when_dao_fails() {
        mockDAO();
        when(trackDAO.list("id")).thenReturn(Single.error(exception));

        subjectUnderTesting.list("id")
                .test()
                .assertNotComplete()
                .assertError(error -> error == exception);
    }

    private void mockDAO() {
        when(appDataBase.trackDAO()).thenReturn(trackDAO);
    }

    @Test
    public void test_save_it_should_complete() {
        List<Track> tracks = new ArrayList<>();
        mockDAO();
        subjectUnderTesting.save(tracks, "id")
                .test()
                .assertComplete();

        verify(trackDAO).save(anyList());
    }

    @Test
    public void test_save_it_should_fail_when_dao_fails() {
        List<Track> tracks = new ArrayList<>();
        mockDAO();
        doThrow(exception).when(trackDAO).save(anyList());
        subjectUnderTesting.save(tracks, "id")
                .test()
                .assertNotComplete()
                .assertError(error -> error == exception);
    }

    @Test
    public void test_deleteTracks_it_should_complete() {
        when(album.getId()).thenReturn("id");
        mockDAO();
        subjectUnderTesting.deleteTracks(album)
                .test()
                .assertComplete();
        verify(trackDAO).delete("id");
    }


    @Test
    public void test_deleteTracks_it_should_fail() {
        when(album.getId()).thenReturn("id");
        mockDAO();
        doThrow(exception).when(trackDAO).delete("id");
        subjectUnderTesting.deleteTracks(album)
                .test()
                .assertNotComplete();
        verify(trackDAO).delete("id");
    }
}