package com.ceduliocezar.lastfm.model.interactor.album;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.repository.album.AlbumRepository;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.Mockito.when;


/**
 * Test suite for {@link GetFavoriteAlbums}
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(AppLog.class)
public class GetFavoriteAlbumsTest {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private GetFavoriteAlbums subjectUnderTesting;

    @Mock
    private AlbumRepository albumRepository;

    @Mock
    private Throwable exception;

    @Test
    public void test_buildSingle_it_should_return_the_same_album_list() {
        List<Album> albumList = new ArrayList<>();
        when(albumRepository.favoriteAlbums()).thenReturn(Single.just(albumList));

        TestObserver<List<Album>> testObserver = subjectUnderTesting.buildSingle(null).test();

        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertValue(result -> result == albumList);
    }

    @Test
    public void test_buildSingle_it_should_fail_when_repository_fails() {

        when(albumRepository.favoriteAlbums()).thenReturn(Single.error(exception));

        TestObserver<List<com.ceduliocezar.lastfm.model.entity.Album>> testObserver = subjectUnderTesting.buildSingle(null).test();

        testObserver.awaitTerminalEvent();
        testObserver.assertNotComplete();
        testObserver.assertError(error -> error == exception);
    }

}