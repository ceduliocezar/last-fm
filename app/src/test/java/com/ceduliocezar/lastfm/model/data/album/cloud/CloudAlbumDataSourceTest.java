package com.ceduliocezar.lastfm.model.data.album.cloud;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.data.APIService;
import com.ceduliocezar.lastfm.model.data.artist.cloud.Artist;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.eq;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;


/**
 * Test suite for {@link CloudAlbumDataSource}
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(AppLog.class)
public class CloudAlbumDataSourceTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private CloudAlbumDataSource subjectUnderTesting;

    @Mock
    private APIService apiService;

    @Mock
    private Throwable exception;

    @Mock
    private Album album;

    @Mock
    private Artist artist;

    @Mock
    private com.ceduliocezar.lastfm.model.entity.Album entityAlbum;


    @Before
    public void setUp() {
        mockStatic(AppLog.class);
    }

    @Test
    public void test_topAlbums_it_should_return_same_album_list_when_succeed() {
        String artistId = "c3cceeed-3332-4cf0-8c4c-bbde425147b6";
        List<Album> albumList = new ArrayList<>();
        albumList.add(album);
        when(album.getArtist()).thenReturn(artist);
        when(artist.getId()).thenReturn("artistId");
        when(album.getId()).thenReturn("id");
        when(album.getName()).thenReturn("name");

        when(apiService.topAlbums(eq(artistId))).thenReturn(Single.just(albumList));
        TestObserver<List<com.ceduliocezar.lastfm.model.entity.Album>> testObserver = subjectUnderTesting.topAlbums(artistId).test();

        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertValue(result -> {
            com.ceduliocezar.lastfm.model.entity.Album album = result.get(0);
            return result.size() == 1 && album.getName().equals("name") && album.getArtistId().equals("artistId") && album.getId().equals("id");
        });
    }

    @Test
    public void test_topAlbums_it_should_not_complete_when_a_fail_occurs() {
        String artistId = "c3cceeed-3332-4cf0-8c4c-bbde425147b6";
        when(apiService.topAlbums(eq(artistId))).thenReturn(Single.error(exception));
        TestObserver<List<com.ceduliocezar.lastfm.model.entity.Album>> testObserver = subjectUnderTesting.topAlbums(artistId).test();

        testObserver.awaitTerminalEvent();
        testObserver.assertNotComplete();
        testObserver.assertError(error -> error == exception);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void test_save_it_should_fail() {
        subjectUnderTesting.save(entityAlbum);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void test_favoriteAlbums_it_should_fail() {
        subjectUnderTesting.favoriteAlbums();
    }

    @Test(expected = UnsupportedOperationException.class)
    public void test_delete_it_should_fail() {
        subjectUnderTesting.delete(entityAlbum);
    }

    @Test(expected = UnsupportedOperationException.class)
    public void getAlbum_it_should_fail() {
        subjectUnderTesting.getAlbum("id");
    }
}