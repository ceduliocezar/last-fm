package com.ceduliocezar.lastfm.model.interactor.artist;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.entity.Artist;
import com.ceduliocezar.lastfm.model.repository.artist.ArtistRepository;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Test suit for {@link GetArtistById}
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(AppLog.class)
public class GetArtistByIdTest {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private GetArtistById subjectUnderTesting;

    @Mock
    private ArtistRepository artistRepository;

    @Mock
    private Throwable exception;

    @Mock
    private Artist artist;

    @Test
    public void test_buildSingle_it_should_complete() {

        when(artistRepository.get(anyString())).thenReturn(Single.just(artist));

        TestObserver<Artist> testObserver = subjectUnderTesting.buildSingle("artist_id").test();

        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertValue(result -> result == artist);
    }

    @Test
    public void test_buildSingle_it_should_fail_when_repository_fails() {

        when(artistRepository.get(anyString())).thenReturn(Single.error(exception));

        TestObserver<Artist> testObserver = subjectUnderTesting.buildSingle("artist_id").test();

        testObserver.awaitTerminalEvent();
        testObserver.assertNotComplete();
        testObserver.assertError(error -> error == exception);
    }

}