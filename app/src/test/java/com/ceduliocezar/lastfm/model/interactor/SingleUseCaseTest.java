package com.ceduliocezar.lastfm.model.interactor;

import com.ceduliocezar.lastfm.di.threading.ExecutionThread;
import com.ceduliocezar.lastfm.di.threading.PostExecutionThread;
import com.ceduliocezar.lastfm.logging.AppLog;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest(AppLog.class)
public class SingleUseCaseTest {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Mock
    private PostExecutionThread postExecutionThread;

    @Mock
    private ExecutionThread executionThread;

    private CompositeDisposable compositeDisposable;

    @Mock
    private Throwable exception;
    private String result;

    private Throwable resultException;

    @Before
    public void setUp() {
        compositeDisposable = new CompositeDisposable(); // not able to mock this final class, even with power mock
        mockStatic(AppLog.class);
        result = "wrongResult";
        resultException = null;
    }

    @Test
    public void test_execute_success_it_should_deliver_the_same_string() {
        when(postExecutionThread.getScheduler()).thenReturn(Schedulers.trampoline());
        when(executionThread.getScheduler()).thenReturn(Schedulers.trampoline());

        SingleUseCase<String, String> subjectUnderTesting = new SingleUseCase<String, String>(postExecutionThread, executionThread, compositeDisposable) {
            @Override
            protected Single<String> buildSingle(String param) {
                return Single.just(param);
            }
        };

        subjectUnderTesting.execute("scorpions0", new DisposableSingleObserver<String>() {
            @Override
            public void onSuccess(String s) {
                result = s;
            }

            @Override
            public void onError(Throwable e) {

            }
        });

        assertEquals("scorpions0", result);
    }

    @Test
    public void test_execute_error_it_should_deliver_the_same_error() {
        when(postExecutionThread.getScheduler()).thenReturn(Schedulers.trampoline());
        when(executionThread.getScheduler()).thenReturn(Schedulers.trampoline());

        SingleUseCase<String, String> singleUseCase = new SingleUseCase<String, String>(postExecutionThread, executionThread, compositeDisposable) {
            @Override
            protected Single<String> buildSingle(String param) {
                return Single.error(exception);
            }
        };

        singleUseCase.execute("scorpions0", new DisposableSingleObserver<String>() {
            @Override
            public void onSuccess(String s) {

            }

            @Override
            public void onError(Throwable e) {
                resultException = e;
            }
        });

        assertEquals(exception, resultException);
    }

}