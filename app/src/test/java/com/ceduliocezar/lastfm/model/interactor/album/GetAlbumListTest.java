package com.ceduliocezar.lastfm.model.interactor.album;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.repository.album.AlbumRepository;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(AppLog.class)
public class GetAlbumListTest {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private GetAlbumList getAlbumList;
    @Mock
    private com.ceduliocezar.lastfm.model.entity.Album album;

    @Mock
    private AlbumRepository albumRepository;

    @Mock
    private Throwable exception;

    @Test
    public void test_buildSingle_it_should_return_the_same_album_list() {
        String artistId = "c3cceeed-3332-4cf0-8c4c-bbde425147b6";
        List<com.ceduliocezar.lastfm.model.entity.Album> albumList = new ArrayList<>();
        albumList.add(album);
        when(albumRepository.topAlbums(eq(artistId))).thenReturn(Single.just(albumList));

        TestObserver<List<com.ceduliocezar.lastfm.model.entity.Album>> testObserver = getAlbumList.buildSingle(artistId).test();

        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertValue(result -> result == albumList);
    }

    @Test
    public void test_buildSingle_it_should_fail_when_repository_fails() {
        String artistId = "c3cceeed-3332-4cf0-8c4c-bbde425147b6";
        when(albumRepository.topAlbums(eq(artistId))).thenReturn(Single.error(exception));

        TestObserver<List<com.ceduliocezar.lastfm.model.entity.Album>> testObserver = getAlbumList.buildSingle(artistId).test();

        testObserver.awaitTerminalEvent();
        testObserver.assertNotComplete();
        testObserver.assertError(error -> error == exception);
    }

}