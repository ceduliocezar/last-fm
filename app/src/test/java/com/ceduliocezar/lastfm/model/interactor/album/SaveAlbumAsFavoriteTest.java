package com.ceduliocezar.lastfm.model.interactor.album;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.interactor.artist.SaveArtist;
import com.ceduliocezar.lastfm.model.interactor.image.SaveAlbumImage;
import com.ceduliocezar.lastfm.model.interactor.track.SaveTracks;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import io.reactivex.Completable;
import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.any;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Test suite for {@link SaveAlbumAsFavorite}
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({AppLog.class})
public class SaveAlbumAsFavoriteTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private SaveAlbumAsFavorite subjectUnderTesting;

    @Mock
    private Throwable exception;

    @Mock
    private com.ceduliocezar.lastfm.model.entity.Album album;

    @Mock
    private SaveAlbumAsFavorite.Request request;

    @Mock
    private SaveArtist saveArtist;

    @Mock
    private SaveTracks saveTracks;

    @Mock
    private SaveAlbum saveAlbum;

    @Mock
    private SaveAlbumImage saveAlbumImage;

    @Before
    public void setUp() {
        mockStatic(AppLog.class);
    }

    @Test
    public void test_buildCompletable_it_should_emit_complete() {
        when(request.getAlbum()).thenReturn(album);
        when(saveArtist.buildCompletable(any())).thenReturn(Completable.complete());
        when(saveAlbum.buildCompletable(any())).thenReturn(Completable.complete());
        when(saveTracks.buildCompletable(any())).thenReturn(Completable.complete());
        when(saveAlbumImage.buildCompletable(any())).thenReturn(Completable.complete());

        TestObserver<Void> testObserver = subjectUnderTesting.buildCompletable(request).test();

        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
    }

    @Test
    public void test_buildCompletable_it_should_emit_error() {
        when(request.getAlbum()).thenReturn(album);
        when(saveArtist.buildCompletable(any())).thenReturn(Completable.complete());
        when(saveAlbum.buildCompletable(any())).thenReturn(Completable.error(exception));
        when(saveTracks.buildCompletable(any())).thenReturn(Completable.error(exception));
        when(saveAlbumImage.buildCompletable(any())).thenReturn(Completable.error(exception));

        TestObserver<Void> testObserver = subjectUnderTesting.buildCompletable(request).test();

        testObserver.awaitTerminalEvent();
        testObserver.assertNotComplete();
        testObserver.assertError(error -> error == exception);

    }
}