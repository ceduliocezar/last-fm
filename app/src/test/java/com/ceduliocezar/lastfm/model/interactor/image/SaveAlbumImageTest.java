package com.ceduliocezar.lastfm.model.interactor.image;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.entity.Image;
import com.ceduliocezar.lastfm.model.repository.image.ImageRepository;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import io.reactivex.Completable;
import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/**
 * Test suite for {@link SaveAlbumImage}
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(AppLog.class)
public class SaveAlbumImageTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private SaveAlbumImage subjectUnderTesting;

    @Mock
    private ImageRepository imageRepository;

    @Mock
    private Throwable exception;

    @Mock
    private Image image;


    @Test
    public void test_buildSingle_it_should_complete() {

        when(imageRepository.saveAlbumImage(eq(image))).thenReturn(Completable.complete());

        TestObserver<Void> testObserver = subjectUnderTesting.buildCompletable(image).test();

        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
    }

    @Test
    public void test_buildSingle_it_should_fail_when_repository_fails() {

        when(imageRepository.saveAlbumImage(eq(image))).thenReturn(Completable.error(exception));

        TestObserver<Void> testObserver = subjectUnderTesting.buildCompletable(image).test();

        testObserver.awaitTerminalEvent();
        testObserver.assertNotComplete();
        testObserver.assertError(error -> error == exception);
    }

}