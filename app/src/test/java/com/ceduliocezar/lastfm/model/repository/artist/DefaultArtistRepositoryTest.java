package com.ceduliocezar.lastfm.model.repository.artist;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.data.artist.ArtistDataSource;
import com.ceduliocezar.lastfm.model.entity.Artist;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

/**
 * Test suite for {@link DefaultArtistRepository}
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(AppLog.class)
public class DefaultArtistRepositoryTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    private DefaultArtistRepository subjectUnderTesting;

    @Mock
    private ArtistDataSource localDataSource;

    @Mock
    private ArtistDataSource cloudDataSource;

    @Mock
    private Throwable throwable;

    @Mock
    private Artist artist;

    @Mock
    private Throwable exception;

    @Mock
    private Artist artistCloud;

    @Before
    public void setUp() {
        mockStatic(AppLog.class);
        subjectUnderTesting = new DefaultArtistRepository(cloudDataSource, localDataSource);
    }

    @Test
    public void test_list_it_should_return_the_same_result_as_data_source() {
        List<com.ceduliocezar.lastfm.model.entity.Artist> artistList = new ArrayList<>();
        when(cloudDataSource.list(eq("scorp"))).thenReturn(Single.just(artistList));
        TestObserver<List<com.ceduliocezar.lastfm.model.entity.Artist>> testObserver = subjectUnderTesting.list("scorp").test();
        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertValue(result -> result == artistList);
    }

    @Test
    public void test_list_it_should_return_error_when_an_error_occurs() {
        when(cloudDataSource.list(eq("scorp"))).thenReturn(Single.error(throwable));
        TestObserver<List<com.ceduliocezar.lastfm.model.entity.Artist>> testObserver = subjectUnderTesting.list("scorp").test();
        testObserver.awaitTerminalEvent();
        testObserver.assertNotComplete();
        testObserver.assertError(resultError -> resultError == throwable);
    }

    @Test
    public void test_save_it_should_complete() {
        when(localDataSource.save(artist)).thenReturn(Completable.complete());
        TestObserver<Void> testObserver = subjectUnderTesting.save(artist).test();
        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
    }

    @Test
    public void test_save_it_should_fail() {
        when(localDataSource.save(artist)).thenReturn(Completable.complete());
        TestObserver<Void> testObserver = subjectUnderTesting.save(artist).test();
        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
    }

    @Test
    public void test_get_it_should_return_same_result_as_local_data_source() {
        when(localDataSource.getArtist("artistId")).thenReturn(Single.just(artist));
        TestObserver<Artist> testObserver = subjectUnderTesting.get("artistId").test();
        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertValue(result -> result == artist);
    }

    @Test
    public void test_get_it_should_try_to_load_from_cloud_when_local_fails() {
        when(localDataSource.getArtist("artistId")).thenReturn(Single.error(exception));
        when(cloudDataSource.getArtist("artistId")).thenReturn(Single.just(artistCloud));
        TestObserver<Artist> testObserver = subjectUnderTesting.get("artistId").test();
        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertValue(result -> result == artistCloud);
    }
}