package com.ceduliocezar.lastfm.model.repository.image;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.data.image.ImageDataSource;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Image;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

/**
 * Test suit for {@link DefaultImageRepository}
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(AppLog.class)
public class DefaultImageRepositoryTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    private DefaultImageRepository defaultImageRepository;

    @Mock
    private ImageDataSource localImageDataSource;

    @Mock
    private ImageDataSource cloudImageDataSource;

    @Mock
    private Image image;

    @Mock
    private Throwable exception;

    @Mock
    private Image cloudImage;

    @Mock
    private Album album;


    @Before
    public void setUp() {
        mockStatic(AppLog.class);
        defaultImageRepository = new DefaultImageRepository(localImageDataSource, cloudImageDataSource);
    }

    @Test
    public void test_getAlbumImage_it_should_complete_from_local() {
        when(localImageDataSource.getImage("id")).thenReturn(Single.just(image));
        when(cloudImageDataSource.getImage("id")).thenReturn(Single.just(cloudImage));
        TestObserver<Image> testObserver = defaultImageRepository.getAlbumImage("id").test();
        testObserver.assertComplete();
        testObserver.assertValue(result -> result == image);
    }

    @Test
    public void test_getAlbumImage_it_should_call_cloud_as_fallback() {
        when(localImageDataSource.getImage("id")).thenReturn(Single.error(exception));
        when(cloudImageDataSource.getImage("id")).thenReturn(Single.just(cloudImage));
        TestObserver<Image> testObserver = defaultImageRepository.getAlbumImage("id").test();
        testObserver.assertComplete();
        testObserver.assertValue(result -> result == cloudImage);
    }

    @Test
    public void test_getAlbumImage_it_should_fail_when_both_data_sources_fail() {
        when(localImageDataSource.getImage("id")).thenReturn(Single.error(exception));
        when(cloudImageDataSource.getImage("id")).thenReturn(Single.error(exception));
        TestObserver<Image> testObserver = defaultImageRepository.getAlbumImage("id").test();
        testObserver.assertNotComplete();
        testObserver.assertError(error -> error == exception);
    }

    @Test
    public void test_saveAlbumImage_it_should_complete() {
        when(localImageDataSource.saveAlbumImage(image)).thenReturn(Completable.complete());
        TestObserver<Void> testObserver = defaultImageRepository.saveAlbumImage(image).test();
        testObserver.assertComplete();
    }

    @Test
    public void test_saveAlbumImage_it_should_fail_when_local_data_source_fail() {
        when(localImageDataSource.saveAlbumImage(image)).thenReturn(Completable.error(exception));
        TestObserver<Void> testObserver = defaultImageRepository.saveAlbumImage(image).test();
        testObserver.assertNotComplete();
        testObserver.assertError(error -> error == exception);
    }

    @Test
    public void test_deleteAlbumImage_it_should_complete() {
        when(localImageDataSource.deleteImage(album)).thenReturn(Completable.complete());
        TestObserver<Void> testObserver = defaultImageRepository.deleteAlbumImage(album).test();
        testObserver.assertComplete();
    }

    @Test
    public void test_deleteAlbumImage_it_should_fail_when_local_fails() {
        when(localImageDataSource.deleteImage(album)).thenReturn(Completable.error(exception));
        TestObserver<Void> testObserver = defaultImageRepository.deleteAlbumImage(album).test();
        testObserver.assertNotComplete();
        testObserver.assertError(error -> error == exception);
    }
}