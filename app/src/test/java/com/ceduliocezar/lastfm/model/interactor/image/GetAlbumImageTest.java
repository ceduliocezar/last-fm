package com.ceduliocezar.lastfm.model.interactor.image;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Image;
import com.ceduliocezar.lastfm.model.repository.image.ImageRepository;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

/**
 * Test suite for {@link GetAlbumImage}
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest(AppLog.class)
public class GetAlbumImageTest {
    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private GetAlbumImage subjectUnderTesting;

    @Mock
    private ImageRepository imageRepository;

    @Mock
    private Throwable exception;

    @Mock
    private Image image;

    @Mock
    private Album album;

    @Test
    public void test_buildSingle_it_should_complete() {
        when(album.getId()).thenReturn("albumId");
        when(imageRepository.getAlbumImage(anyString())).thenReturn(Single.just(image));

        TestObserver<Image> testObserver = subjectUnderTesting.buildSingle(album).test();

        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertValue(result -> result == image);
    }

    @Test
    public void test_buildSingle_it_should_fail_when_repository_fails() {
        when(album.getId()).thenReturn("albumId");
        when(imageRepository.getAlbumImage(anyString())).thenReturn(Single.error(exception));

        TestObserver<Image> testObserver = subjectUnderTesting.buildSingle(album).test();

        testObserver.awaitTerminalEvent();
        testObserver.assertNotComplete();
        testObserver.assertError(error -> error == exception);
    }

}