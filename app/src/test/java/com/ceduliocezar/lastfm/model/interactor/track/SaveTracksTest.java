package com.ceduliocezar.lastfm.model.interactor.track;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.entity.Track;
import com.ceduliocezar.lastfm.model.repository.track.TrackRepository;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;


@RunWith(PowerMockRunner.class)
@PrepareForTest(AppLog.class)
public class SaveTracksTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private SaveTracks subjectUnderTesting;

    @Mock
    private TrackRepository trackRepository;

    @Mock
    private Throwable exception;

    @Mock
    private SaveTracks.Request request;


    @Test
    public void test_buildSingle_it_should_complete() {

        List<Track> tracks = new ArrayList<>();
        when(request.getTracks()).thenReturn(tracks);
        when(request.getAlbumId()).thenReturn("id");
        when(trackRepository.saveAlbumTracks(tracks, "id")).thenReturn(Completable.complete());

        TestObserver<Void> testObserver = subjectUnderTesting.buildCompletable(request).test();

        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
    }

    @Test
    public void test_buildSingle_it_should_fail_when_repository_fails() {
        List<Track> tracks = new ArrayList<>();
        when(request.getTracks()).thenReturn(tracks);
        when(request.getAlbumId()).thenReturn("id");
        when(trackRepository.saveAlbumTracks(any(), any())).thenReturn(Completable.error(exception));

        TestObserver<Void> testObserver = subjectUnderTesting.buildCompletable(request).test();

        testObserver.awaitTerminalEvent();
        testObserver.assertNotComplete();
        testObserver.assertError(error -> error == exception);
    }

}