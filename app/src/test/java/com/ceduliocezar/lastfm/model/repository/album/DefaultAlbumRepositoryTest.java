package com.ceduliocezar.lastfm.model.repository.album;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.data.album.AlbumDataSource;
import com.ceduliocezar.lastfm.model.entity.Album;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;
import io.reactivex.observers.TestObserver;

import static org.mockito.ArgumentMatchers.eq;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.verifyZeroInteractions;
import static org.powermock.api.mockito.PowerMockito.when;

@RunWith(PowerMockRunner.class)
@PrepareForTest(AppLog.class)
public class DefaultAlbumRepositoryTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    private DefaultAlbumRepository subjectUnderTesting;

    @Mock
    private AlbumDataSource cloudDataSource;

    @Mock
    private AlbumDataSource localDataSource;

    @Mock
    private com.ceduliocezar.lastfm.model.entity.Album album;

    @Mock
    private Throwable exception;


    @Before
    public void setUp() {
        mockStatic(AppLog.class);
        subjectUnderTesting = new DefaultAlbumRepository(cloudDataSource, localDataSource);
    }

    @Test
    public void test_topAlbums_it_should_return_same_list_when_search_succeed() {
        String artistId = "c3cceeed-3332-4cf0-8c4c-bbde425147b6";
        List<com.ceduliocezar.lastfm.model.entity.Album> albumList = new ArrayList<>();
        albumList.add(album);
        when(cloudDataSource.topAlbums(eq(artistId))).thenReturn(Single.just(albumList));

        TestObserver<List<com.ceduliocezar.lastfm.model.entity.Album>> testObserver = subjectUnderTesting.topAlbums(artistId).test();
        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        testObserver.assertValue(result -> result == albumList);
    }

    @Test
    public void test_topAlbums_it_should_not_complete_when_an_error_occurs() {
        String artistId = "c3cceeed-3332-4cf0-8c4c-bbde425147b6";
        when(cloudDataSource.topAlbums(eq(artistId))).thenReturn(Single.error(exception));

        TestObserver<List<com.ceduliocezar.lastfm.model.entity.Album>> testObserver = subjectUnderTesting.topAlbums(artistId).test();
        testObserver.awaitTerminalEvent();
        testObserver.assertNotComplete();
        testObserver.assertError(error -> error == exception);
    }

    @Test
    public void test_save_it_should_save_using_local_data_source() {
        when(localDataSource.save(album)).thenReturn(Completable.complete());
        TestObserver<Void> testObserver = subjectUnderTesting.save(album).test();
        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
        verifyZeroInteractions(cloudDataSource);
    }


    @Test
    public void test_favoriteAlbums_it_should_complete() {
        List<com.ceduliocezar.lastfm.model.entity.Album> albumList = new ArrayList<>();
        albumList.add(album);
        when(localDataSource.favoriteAlbums()).thenReturn(Single.just(albumList));
        TestObserver<List<com.ceduliocezar.lastfm.model.entity.Album>> testObserver = subjectUnderTesting.favoriteAlbums().test();
        testObserver.assertComplete();
        testObserver.assertValue(result -> result == albumList);
    }

    @Test
    public void test_favoriteAlbums_it_should_fail_when_data_source_fails() {
        when(localDataSource.favoriteAlbums()).thenReturn(Single.error(exception));
        TestObserver<List<com.ceduliocezar.lastfm.model.entity.Album>> testObserver = subjectUnderTesting.favoriteAlbums().test();
        testObserver.awaitTerminalEvent();
        testObserver.assertNotComplete();
        testObserver.assertError(error -> error == exception);
    }

    @Test
    public void test_delete_it_should_complete() {
        when(localDataSource.delete(eq(album))).thenReturn(Completable.complete());
        TestObserver<Void> testObserver = subjectUnderTesting.delete(album).test();
        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
    }

    @Test
    public void test_delete_it_should_fail_when_local_data_source_fails() {
        when(localDataSource.delete(eq(album))).thenReturn(Completable.error(exception));
        TestObserver<Void> testObserver = subjectUnderTesting.delete(album).test();
        testObserver.awaitTerminalEvent();
        testObserver.assertNotComplete();
        testObserver.assertError(error -> error == exception);
    }

    @Test
    public void test_getAlbum_it_should_complete() {
        when(localDataSource.getAlbum(eq("id"))).thenReturn(Single.just(album));
        TestObserver<Album> testObserver = subjectUnderTesting.getAlbum("id").test();
        testObserver.awaitTerminalEvent();
        testObserver.assertComplete();
    }

    @Test
    public void test_getAlbum_it_should_fail_when_local_data_source_fails() {
        when(localDataSource.getAlbum(eq("id"))).thenReturn(Single.error(exception));
        TestObserver<Album> testObserver = subjectUnderTesting.getAlbum("id").test();
        testObserver.awaitTerminalEvent();
        testObserver.assertNotComplete();
        testObserver.assertError(error -> error == exception);
    }
}