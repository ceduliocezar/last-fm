package com.ceduliocezar.lastfm.presentation.album.detail;

import android.support.test.espresso.idling.CountingIdlingResource;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Artist;
import com.ceduliocezar.lastfm.model.entity.Image;
import com.ceduliocezar.lastfm.model.entity.Track;
import com.ceduliocezar.lastfm.model.interactor.album.DeleteAlbum;
import com.ceduliocezar.lastfm.model.interactor.album.SaveAlbumAsFavorite;
import com.ceduliocezar.lastfm.model.interactor.album.VerifyFavorite;
import com.ceduliocezar.lastfm.model.interactor.artist.GetArtistById;
import com.ceduliocezar.lastfm.model.interactor.image.GetAlbumImage;
import com.ceduliocezar.lastfm.model.interactor.track.GetAlbumTracks;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({AppLog.class, CountingIdlingResource.class})
public class AlbumDetailPresenterTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private AlbumDetailPresenter subjectUnderTesting;

    @Mock
    private SaveAlbumAsFavorite saveAlbum;
    @Mock
    private DeleteAlbum deleteAlbum;
    @Mock
    private GetArtistById getArtistById;
    @Mock
    private GetAlbumTracks getAlbumTracks;
    @Mock
    private GetAlbumImage getAlbumImage;
    @Mock
    private VerifyFavorite verifyFavorite;

    @Mock
    private AlbumDetailContract.View view;

    @Mock
    private Album album;

    @Mock
    private Artist artist;

    @Mock
    private Image albumImage;
    @Captor
    private ArgumentCaptor<DisposableCompletableObserver> completableObserver;

    @Mock
    private Throwable exception;

    @Mock
    @SuppressWarnings("unused")
    private CountingIdlingResource countingIdlingResource;

    @Captor
    private ArgumentCaptor<DisposableSingleObserver<Image>> singleImageCaptor;

    @Mock
    private Image image;

    @Captor
    private ArgumentCaptor<DisposableSingleObserver<List<Track>>> tracksCaptor;
    @Captor
    private ArgumentCaptor<DisposableSingleObserver<Boolean>> booleanCaptor;

    @Before
    public void setUp() {
        mockStatic(AppLog.class);
    }

    @Test
    public void test_onViewCreated_it_should_hold_view_reference() {
        when(view.getAlbumParam()).thenReturn(album);
        subjectUnderTesting.onViewCreated(view);

        assertNotNull(subjectUnderTesting.getView());
    }

    @Test
    public void test_onViewCreated_it_should_start_loading_properties() {
        when(view.getAlbumParam()).thenReturn(album);

        subjectUnderTesting.onViewCreated(view);

        getArtistById.execute(any(), any());
        getAlbumTracks.execute(any(), any());
        getAlbumImage.execute(any(), any());
        verifyFavorite.execute(any(), any());
        verify(view).displayAlbum(album);
    }

    @Test
    public void test_onViewDestroyed_it_should_release_view_reference() {
        subjectUnderTesting.onViewDestroyed();
        assertNull(subjectUnderTesting.getView());
    }

    @Test
    public void test_onViewDestroyed_it_should_dispose_use_cases() {
        subjectUnderTesting.onViewDestroyed();

        verify(getArtistById).dispose();
        verify(getAlbumTracks).dispose();
        verify(getAlbumImage).dispose();
        verify(verifyFavorite).dispose();
    }

    @Test
    public void test_userWantsToSaveAlbumAsFavorite_it_should_start_save() {
        List<Track> tracks = new ArrayList<>();
        subjectUnderTesting.userWantsToSaveAlbumAsFavorite(album, artist, albumImage, tracks);
        verify(saveAlbum).execute(any(), any());
    }

    @Test
    public void test_save_it_should_notify_album_saved() {
        List<Track> tracks = new ArrayList<>();
        subjectUnderTesting.saveAlbum(album, artist, albumImage, tracks);
        verify(saveAlbum).execute(any(), completableObserver.capture());
        completableObserver.getValue().onComplete();
        verify(view).notifyAlbumSaved();
    }

    @Test
    public void test_save_it_should_notify_error_saving() {
        List<Track> tracks = new ArrayList<>();
        subjectUnderTesting.saveAlbum(album, artist, albumImage, tracks);
        verify(saveAlbum).execute(any(), completableObserver.capture());
        completableObserver.getValue().onError(exception);
        verify(view).warnErrorWhileSaving();
    }

    @Test
    public void test_userWantsToRemoveAlbumAsFavorite_it_should_delegate_to_delete_use_case() {
        subjectUnderTesting.userWantsToRemoveAlbumAsFavorite(album);
        verify(deleteAlbum).execute(any(), any());
    }

    @Test
    public void test_deleteAlbum_it_should_notify_removal() {
        subjectUnderTesting.deleteAlbum(album);
        verify(deleteAlbum).execute(any(), completableObserver.capture());
        completableObserver.getValue().onComplete();
        verify(view).notifyAlbumRemoved();
    }

    @Test
    public void test_deleteAlbum_it_should_notify_error() {
        subjectUnderTesting.deleteAlbum(album);
        verify(deleteAlbum).execute(any(), completableObserver.capture());
        completableObserver.getValue().onError(exception);
        verify(view).warnErrorRemovingAlbum();
    }

    @Test
    public void test_loadAlbumImage_it_should_display_the_result() {
        subjectUnderTesting.loadAlbumImage(album);

        verify(getAlbumImage).execute(eq(album), singleImageCaptor.capture());
        singleImageCaptor.getValue().onSuccess(image);
        verify(view).displayImage(eq(image));
    }

    @Test
    public void test_loadTracks_it_should_display_tracks() {
        List<Track> tracks = new ArrayList<>();
        subjectUnderTesting.loadTracks(album);
        verify(getAlbumTracks).execute(any(), tracksCaptor.capture());
        tracksCaptor.getValue().onSuccess(tracks);
        verify(view).displayTracks(tracks);
    }

    @Test
    public void test_loadFavorite_it_should_display_album_favorite() {
        subjectUnderTesting.loadFavorite(album);
        verify(verifyFavorite).execute(eq(null), booleanCaptor.capture());
        booleanCaptor.getValue().onSuccess(true);
        verify(view).displayFavorite(true);
    }
}