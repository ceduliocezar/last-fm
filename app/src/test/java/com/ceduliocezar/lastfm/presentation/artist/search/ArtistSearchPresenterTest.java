package com.ceduliocezar.lastfm.presentation.artist.search;

import android.support.test.espresso.idling.CountingIdlingResource;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.entity.Artist;
import com.ceduliocezar.lastfm.model.interactor.artist.GetArtistList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.observers.DisposableSingleObserver;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

/**
 * Test suite for {@link ArtistSearchPresenter}
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({AppLog.class, CountingIdlingResource.class})
public class ArtistSearchPresenterTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private ArtistSearchPresenter subjectUnderTesting;

    @Mock
    private ArtistSearchContract.View view;

    @Mock
    private GetArtistList getArtistList;

    @Captor
    private ArgumentCaptor<DisposableSingleObserver<List<Artist>>> disposableCaptor;

    @Mock
    private Artist artist;

    @Mock
    private Throwable exception;

    @Mock
    @SuppressWarnings("unused")
    private CountingIdlingResource countingIdlingResource;

    @Before
    public void setUp() {
        mockStatic(AppLog.class);
    }

    @Test
    public void test_onViewCreated_it_should_attach_to_view() {
        subjectUnderTesting.onViewCreated(view);
        assertNotNull(subjectUnderTesting.getView());
    }

    @Test
    public void test_onViewCreated_it_should_show_artists_when_there_are_results() {
        List<Artist> artists = new ArrayList<>();
        artists.add(artist);
        subjectUnderTesting.onViewCreated(view);
        subjectUnderTesting.onUserSearched("scorpions");
        verify(getArtistList).execute(eq("scorpions"), disposableCaptor.capture());
        disposableCaptor.getValue().onSuccess(artists);
        verify(view).showArtists(artists);
    }

    @Test
    public void test_onViewCreated_it_should_warn_when_an_error_occurs() {
        subjectUnderTesting.onViewCreated(view);
        subjectUnderTesting.onUserSearched("scorpions");
        verify(getArtistList).execute(eq("scorpions"), disposableCaptor.capture());
        disposableCaptor.getValue().onError(exception);
        verify(view).warnErrorWhileLoadingArtists();
    }

    @Test
    public void test_userSelected_it_should_navigate_to_top_album_screen() {
        subjectUnderTesting.userSelected(artist);
        verify(view).navigateToTopAlbums(artist);
    }

    @Test
    public void test_onViewDestroyed_it_should_release() {
        subjectUnderTesting.onViewCreated(view);
        subjectUnderTesting.onViewDestroyed();
        assertNull(subjectUnderTesting.getView());
    }
}