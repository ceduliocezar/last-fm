package com.ceduliocezar.lastfm.presentation.album.favorite;

import android.support.test.espresso.idling.CountingIdlingResource;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.interactor.album.GetFavoriteAlbums;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.observers.DisposableSingleObserver;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@RunWith(PowerMockRunner.class)
@PrepareForTest({AppLog.class, CountingIdlingResource.class})
public class FavoriteAlbumPresenterTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private FavoriteAlbumPresenter subjectUnderTesting;

    @Mock
    @SuppressWarnings("unused")
    private CountingIdlingResource countingIdlingResource;

    @Mock
    private GetFavoriteAlbums getFavoriteAlbums;

    @Mock
    private FavoriteAlbumContract.View view;

    @Captor
    private ArgumentCaptor<DisposableSingleObserver<List<Album>>> singleListCaptor;

    @Mock
    private Album album;

    @Before
    public void setUp() {
        mockStatic(AppLog.class);
    }

    @Test
    public void test_onViewCreated_it_should_attach_view() {
        subjectUnderTesting.onViewCreated(view);
        assertNotNull(subjectUnderTesting.getView());
    }

    @Test
    public void test_onViewCreated_it_should_start_load_albums() {
        subjectUnderTesting.onViewCreated(view);
        verify(getFavoriteAlbums).execute(any(), any());
    }

    @Test
    public void test_loadAlbums_it_should_display_results() {
        List<Album> albums = new ArrayList<>();
        albums.add(album);
        subjectUnderTesting.loadAlbums();
        verify(getFavoriteAlbums).execute(any(), singleListCaptor.capture());
        singleListCaptor.getValue().onSuccess(albums);
        verify(view).displayAlbums(albums);
        verify(view).hideEmptyMessage();
    }

    @Test
    public void test_loadAlbums_it_should_show_empty_message() {
        List<Album> albums = new ArrayList<>();

        subjectUnderTesting.loadAlbums();
        verify(getFavoriteAlbums).execute(any(), singleListCaptor.capture());
        singleListCaptor.getValue().onSuccess(albums);
        verify(view).displayAlbums(albums);
        verify(view).displayEmptyMessage();
    }

    @Test
    public void test_onViewDestroy_it_should_release_view_reference() {
        subjectUnderTesting.onViewDestroy();
        assertNull(subjectUnderTesting.getView());
        verify(getFavoriteAlbums).dispose();
    }
}