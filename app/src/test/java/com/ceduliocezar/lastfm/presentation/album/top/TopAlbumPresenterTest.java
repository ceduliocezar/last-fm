package com.ceduliocezar.lastfm.presentation.album.top;

import android.support.test.espresso.idling.CountingIdlingResource;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Artist;
import com.ceduliocezar.lastfm.model.interactor.album.GetAlbumList;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.observers.DisposableSingleObserver;

import static junit.framework.TestCase.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

/**
 * Test suite for {@link TopAlbumPresenter}
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest({AppLog.class, CountingIdlingResource.class})
public class TopAlbumPresenterTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @InjectMocks
    private TopAlbumPresenter subjectUnderTesting;

    @Mock
    private GetAlbumList getAlbumList;

    @Mock
    private TopAlbumContract.View view;

    @Mock
    @SuppressWarnings("unused")
    private CountingIdlingResource countingIdlingResource;

    @Mock
    private Artist artist;

    @Captor
    private ArgumentCaptor<DisposableSingleObserver<List<Album>>> observerCaptor;

    @Mock
    private Album album;

    @Mock
    private Throwable exception;

    @Before
    public void setUp() {
        mockStatic(AppLog.class);
    }

    @Test
    public void test_onViewCreated_it_should_retain_view_reference() {
        when(view.getGetArtistParam()).thenReturn(artist);
        subjectUnderTesting.onViewCreated(view);
        assertNotNull(subjectUnderTesting.getView());
    }

    @Test
    public void test_onViewCreated_it_should_start_loading_albums() {
        String artistId = "c3cceeed-3332-4cf0-8c4c-bbde425147b6";
        when(view.getGetArtistParam()).thenReturn(artist);
        when(artist.getId()).thenReturn(artistId);
        subjectUnderTesting.onViewCreated(view);
        verify(getAlbumList).execute(eq(artistId), any());
    }


    @Test
    public void test_onViewDestroyed_it_should_release_view_reference() {
        subjectUnderTesting.onViewDestroyed();
        assertNull(subjectUnderTesting.getView());
    }

    @Test
    public void test_loadAlbums_it_should_deliver_results_to_view_on_succeed() {
        List<Album> albums = new ArrayList<>();
        albums.add(album);
        String artistId = "c3cceeed-3332-4cf0-8c4c-bbde425147b6";
        when(view.getGetArtistParam()).thenReturn(artist);
        when(artist.getId()).thenReturn(artistId);

        subjectUnderTesting.loadAlbums();

        verify(getAlbumList).execute(eq(artistId), observerCaptor.capture());
        observerCaptor.getValue().onSuccess(albums);
        verify(view).displayAlbumResults(albums);
    }

    @Test
    public void test_loadAlbums_it_should_warn_view_when_error_occurred() {
        String artistId = "c3cceeed-3332-4cf0-8c4c-bbde425147b6";
        when(view.getGetArtistParam()).thenReturn(artist);
        when(artist.getId()).thenReturn(artistId);

        subjectUnderTesting.loadAlbums();

        verify(getAlbumList).execute(eq(artistId), observerCaptor.capture());
        observerCaptor.getValue().onError(exception);
        verify(view).warnErrorLoading();
    }

    @Test
    public void test_userSelectedAlbum_it_should_navigate_to_album_detail() {
        subjectUnderTesting.userSelectedAlbum(album);
        verify(view).navigateToAlbumDetail(eq(album));
    }
}