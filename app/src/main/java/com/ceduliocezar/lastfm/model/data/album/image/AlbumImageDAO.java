package com.ceduliocezar.lastfm.model.data.album.image;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.ceduliocezar.lastfm.model.data.album.AlbumImage;
import com.ceduliocezar.lastfm.model.data.album.cloud.Album;
import com.ceduliocezar.lastfm.model.entity.Image;

import io.reactivex.Single;

@Dao
public interface AlbumImageDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(RoomAlbumImage albumImage);

    @Query("SELECT * FROM album_image WHERE id = :id")
    Single<Image> get(String id);
}
