package com.ceduliocezar.lastfm.model.data;

import java.io.File;

import javax.inject.Inject;

/**
 * Wrapper class of File to ease Unit Testing
 */
public class FileWrapper {

    @Inject
    public FileWrapper() {
        // mandatory di constructor
    }

    public File newFileInstance(File parent, String name) {
        return new File(parent, name);
    }
}
