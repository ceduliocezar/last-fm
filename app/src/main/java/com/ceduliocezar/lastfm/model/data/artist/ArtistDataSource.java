package com.ceduliocezar.lastfm.model.data.artist;

import com.ceduliocezar.lastfm.model.entity.Artist;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Provides access to {@link Artist} entity data through reactive functions.
 */
public interface ArtistDataSource {
    /**
     * @param searchTerm Filter artists.
     * @return All artists available that matches the filter.
     */
    Single<List<Artist>> list(String searchTerm);

    Completable save(Artist artist);

    Single<Artist> getArtist(String artistId);
}
