package com.ceduliocezar.lastfm.model.interactor.album;

import com.ceduliocezar.lastfm.di.threading.ExecutionThread;
import com.ceduliocezar.lastfm.di.threading.PostExecutionThread;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.interactor.SingleUseCase;
import com.ceduliocezar.lastfm.model.repository.album.AlbumRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;

public class GetAlbumList extends SingleUseCase<List<Album>, String> {

    private AlbumRepository albumRepository;

    @Inject
    public GetAlbumList(PostExecutionThread postExecutionThread,
                        ExecutionThread executionThread,
                        CompositeDisposable compositeDisposable,
                        AlbumRepository albumRepository) {
        super(postExecutionThread, executionThread, compositeDisposable);
        this.albumRepository = albumRepository;
    }

    @Override
    protected Single<List<Album>> buildSingle(String param) {
        return albumRepository.topAlbums(param);
    }
}
