package com.ceduliocezar.lastfm.model.interactor.image;

import android.support.annotation.Nullable;

import com.ceduliocezar.lastfm.di.threading.ExecutionThread;
import com.ceduliocezar.lastfm.di.threading.PostExecutionThread;
import com.ceduliocezar.lastfm.model.entity.Image;
import com.ceduliocezar.lastfm.model.interactor.CompletableUseCase;
import com.ceduliocezar.lastfm.model.repository.image.ImageRepository;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.disposables.CompositeDisposable;

public class SaveAlbumImage extends CompletableUseCase<Image> {

    private ImageRepository imageRepository;


    @Inject
    public SaveAlbumImage(PostExecutionThread postExecutionThread,
                          ExecutionThread executionThread,
                          CompositeDisposable compositeDisposable,
                          ImageRepository imageRepository) {
        super(postExecutionThread, executionThread, compositeDisposable);
        this.imageRepository = imageRepository;
    }

    @Override
    public Completable buildCompletable(@Nullable Image image) {
        return imageRepository.saveAlbumImage(image);
    }
}
