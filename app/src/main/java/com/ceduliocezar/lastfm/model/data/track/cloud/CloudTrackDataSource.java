package com.ceduliocezar.lastfm.model.data.track.cloud;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.data.APIService;
import com.ceduliocezar.lastfm.model.data.track.TrackDataSource;
import com.ceduliocezar.lastfm.model.entity.Album;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;

public class CloudTrackDataSource implements TrackDataSource {
    private static final String TAG = "CloudTrackDataSource";

    private APIService apiService;

    @Inject
    public CloudTrackDataSource(APIService apiService) {
        this.apiService = apiService;
    }

    @Override
    public Single<List<com.ceduliocezar.lastfm.model.entity.Track>> list(String albumId) {
        AppLog.d(TAG, "list: " + albumId);
        return apiService.tracks(albumId).map(this::mapToEntity);
    }

    private List<com.ceduliocezar.lastfm.model.entity.Track> mapToEntity(List<Track> tracks) {
        AppLog.d(TAG, "mapToEntity: " + tracks);
        List<com.ceduliocezar.lastfm.model.entity.Track> entityTracks = new ArrayList<>();
        for (Track track : tracks) {
            entityTracks.add(new com.ceduliocezar.lastfm.model.entity.Track(track.getName()));
        }
        return entityTracks;
    }

    @Override
    public Completable save(List<com.ceduliocezar.lastfm.model.entity.Track> tracks, String albumId) {
        throw new UnsupportedOperationException("Save tracks not available on cloud.");
    }

    @Override
    public Completable deleteTracks(Album album) {
        throw new UnsupportedOperationException("Delete tracks not available on cloud");
    }
}
