package com.ceduliocezar.lastfm.model.data.image;


import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Image;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface ImageDataSource {

    Single<Image> getImage(String albumId);

    Completable saveAlbumImage(Image image);

    Completable deleteImage(Album album);
}
