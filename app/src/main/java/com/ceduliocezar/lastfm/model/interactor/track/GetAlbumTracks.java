package com.ceduliocezar.lastfm.model.interactor.track;

import com.ceduliocezar.lastfm.di.threading.ExecutionThread;
import com.ceduliocezar.lastfm.di.threading.PostExecutionThread;
import com.ceduliocezar.lastfm.model.entity.Track;
import com.ceduliocezar.lastfm.model.interactor.SingleUseCase;
import com.ceduliocezar.lastfm.model.repository.track.TrackRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;

public class GetAlbumTracks extends SingleUseCase<List<Track>, String> {

    private TrackRepository trackRepository;

    @Inject
    public GetAlbumTracks(PostExecutionThread postExecutionThread,
                          ExecutionThread executionThread,
                          CompositeDisposable compositeDisposable,
                          TrackRepository trackRepository) {
        super(postExecutionThread, executionThread, compositeDisposable);
        this.trackRepository = trackRepository;
    }

    @Override
    protected Single<List<Track>> buildSingle(String param) {
        return trackRepository.getAlbumTracks(param);
    }
}
