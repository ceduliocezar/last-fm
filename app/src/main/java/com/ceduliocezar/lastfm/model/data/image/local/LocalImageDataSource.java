package com.ceduliocezar.lastfm.model.data.image.local;

import android.content.Context;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.data.AppDataBase;
import com.ceduliocezar.lastfm.model.data.FileWrapper;
import com.ceduliocezar.lastfm.model.data.ImageLoader;
import com.ceduliocezar.lastfm.model.data.album.image.RoomAlbumImage;
import com.ceduliocezar.lastfm.model.data.image.ImageDataSource;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Image;

import java.io.File;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Completable;
import io.reactivex.Single;

public class LocalImageDataSource implements ImageDataSource {

    private static final String TAG = "LocalImageDataSource";
    private AppDataBase appDataBase;
    private Context context;
    private FileWrapper fileWrapper;
    private ImageLoader imageLoader;

    @Inject
    LocalImageDataSource(AppDataBase appDataBase,
                         @Named("applicationContext") Context context,
                         FileWrapper fileWrapper, ImageLoader imageLoader) {
        this.appDataBase = appDataBase;
        this.context = context;
        this.fileWrapper = fileWrapper;
        this.imageLoader = imageLoader;
    }

    @Override
    public Single<Image> getImage(String albumId) {
        return appDataBase.albumImageDAO().get(albumId);
    }

    @Override
    public Completable saveAlbumImage(Image image) {
        return Completable.create(emitter -> {
            File imageFile = getImageFileReference(image.getId());
            boolean fileCreated = imageFile.createNewFile();
            AppLog.d(TAG, "saveAlbumImage: file created:" + fileCreated);

            imageLoader.downloadFile(image.getUrl(), imageFile);
            appDataBase.albumImageDAO().insert(new RoomAlbumImage(imageFile.getAbsolutePath(), image.getId()));
            emitter.onComplete();
        });
    }

    private File getImageFileReference(String imageId) {
        File filesDir = context.getFilesDir();
        return fileWrapper.newFileInstance(filesDir, imageId);
    }

    @Override
    public Completable deleteImage(Album album) {
        return Completable.create(emitter -> {
            File imageFileReference = getImageFileReference(album.getId());
            boolean deleted = imageFileReference.delete();
            AppLog.d(TAG, "subscribe: file deleted:" + deleted);
            emitter.onComplete();
        });
    }
}
