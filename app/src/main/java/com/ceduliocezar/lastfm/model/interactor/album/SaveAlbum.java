package com.ceduliocezar.lastfm.model.interactor.album;

import android.support.annotation.Nullable;

import com.ceduliocezar.lastfm.di.threading.ExecutionThread;
import com.ceduliocezar.lastfm.di.threading.PostExecutionThread;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.interactor.CompletableUseCase;
import com.ceduliocezar.lastfm.model.repository.album.AlbumRepository;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.disposables.CompositeDisposable;

public class SaveAlbum extends CompletableUseCase<Album> {

    private AlbumRepository albumRepository;

    @Inject
    public SaveAlbum(PostExecutionThread postExecutionThread,
                     ExecutionThread executionThread,
                     CompositeDisposable compositeDisposable,
                     AlbumRepository albumRepository) {
        super(postExecutionThread, executionThread, compositeDisposable);
        this.albumRepository = albumRepository;
    }

    @Override
    public Completable buildCompletable(@Nullable Album param) {
        return albumRepository.save(param);
    }
}
