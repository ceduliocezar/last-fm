package com.ceduliocezar.lastfm.model.data.album.cloud;

import com.ceduliocezar.lastfm.model.data.album.AlbumImage;
import com.ceduliocezar.lastfm.model.data.artist.cloud.Artist;
import com.ceduliocezar.lastfm.model.data.track.cloud.Track;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;


public class Album implements Serializable {
    @SerializedName("mbid")
    private String id;
    private String name;
    private Artist artist;
    @SerializedName("image")
    private List<AlbumImage> image;
    @SerializedName("tracks")
    private List<Track> tracks;

    public Album(String id,
                 String name,
                 Artist artist,
                 List<AlbumImage> image,
                 List<Track> tracks) {
        this.id = id;
        this.name = name;
        this.artist = artist;
        this.image = image;
        this.tracks = tracks;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Artist getArtist() {
        return artist;
    }

    public void setArtist(Artist artist) {
        this.artist = artist;
    }

    public List<AlbumImage> getImage() {
        return image;
    }

    public void setImage(List<AlbumImage> image) {
        this.image = image;
    }

    public List<Track> getTracks() {
        return tracks;
    }

    public void setTracks(List<Track> tracks) {
        this.tracks = tracks;
    }

    @Override
    public String toString() {
        return "Album{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", artist=" + artist +
                ", image=" + image +
                ", tracks=" + tracks +
                '}';
    }
}