package com.ceduliocezar.lastfm.model.interactor.album;

import com.ceduliocezar.lastfm.di.threading.ExecutionThread;
import com.ceduliocezar.lastfm.di.threading.PostExecutionThread;
import com.ceduliocezar.lastfm.model.interactor.SingleUseCase;
import com.ceduliocezar.lastfm.model.repository.album.AlbumRepository;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;

public class VerifyFavorite extends SingleUseCase<Boolean, String> {

    private AlbumRepository albumRepository;

    @Inject
    public VerifyFavorite(PostExecutionThread postExecutionThread,
                          ExecutionThread executionThread,
                          CompositeDisposable compositeDisposable,
                          AlbumRepository albumRepository) {
        super(postExecutionThread, executionThread, compositeDisposable);
        this.albumRepository = albumRepository;
    }

    @Override
    protected Single<Boolean> buildSingle(String param) {
        return albumRepository.getAlbum(param)
                .flatMap(album -> Single.just(true));
    }
}
