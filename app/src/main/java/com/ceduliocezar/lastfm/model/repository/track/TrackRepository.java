package com.ceduliocezar.lastfm.model.repository.track;


import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Track;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.CompletableSource;
import io.reactivex.Single;

public interface TrackRepository {
    Single<List<Track>> getAlbumTracks(String albumId);

    Completable saveAlbumTracks(List<Track> tracks, String albumId);

    Completable deleteAlbumTracks(Album album);
}
