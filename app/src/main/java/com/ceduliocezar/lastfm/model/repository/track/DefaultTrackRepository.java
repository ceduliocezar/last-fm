package com.ceduliocezar.lastfm.model.repository.track;

import com.ceduliocezar.lastfm.model.data.track.TrackDataSource;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Track;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Completable;
import io.reactivex.Single;

public class DefaultTrackRepository implements TrackRepository {

    private TrackDataSource localTrackDataSource;
    private TrackDataSource cloudDataSource;

    @Inject
    public DefaultTrackRepository(@Named("localDataSource") TrackDataSource localTrackDataSource,
                                  @Named("cloudDataSource") TrackDataSource cloudDataSource) {
        this.localTrackDataSource = localTrackDataSource;
        this.cloudDataSource = cloudDataSource;
    }

    @Override
    public Single<List<Track>> getAlbumTracks(String albumId) {
        return localTrackDataSource.list(albumId)
                .flatMap(tracks -> {
                    if (tracks.isEmpty()) {
                        return cloudDataSource.list(albumId);
                    }

                    return Single.just(tracks);
                });
    }

    @Override
    public Completable saveAlbumTracks(List<Track> tracks, String albumId) {
        return localTrackDataSource.save(tracks, albumId);
    }

    @Override
    public Completable deleteAlbumTracks(Album album) {
        return localTrackDataSource.deleteTracks(album);
    }
}
