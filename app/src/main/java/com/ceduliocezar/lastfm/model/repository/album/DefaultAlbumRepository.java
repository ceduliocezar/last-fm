package com.ceduliocezar.lastfm.model.repository.album;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.data.album.AlbumDataSource;
import com.ceduliocezar.lastfm.model.entity.Album;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Completable;
import io.reactivex.Single;

public class DefaultAlbumRepository implements AlbumRepository {

    private static final String TAG = "DefaultAlbumRepository";
    private final AlbumDataSource localDataSource;
    private final AlbumDataSource cloudDataSource;

    @Inject
    DefaultAlbumRepository(@Named("cloudDataSource") AlbumDataSource cloudDataSource,
                           @Named("localDataSource") AlbumDataSource localDataSource) {
        this.cloudDataSource = cloudDataSource;
        this.localDataSource = localDataSource;
    }

    @Override
    public Single<List<Album>> topAlbums(String artistId) {
        return cloudDataSource.topAlbums(artistId);
    }

    @Override
    public Completable save(Album album) {
        AppLog.d(TAG, "save: " + album);
        return localDataSource.save(album);
    }

    @Override
    public Single<List<Album>> favoriteAlbums() {
        return localDataSource.favoriteAlbums();
    }

    @Override
    public Completable delete(Album album) {
        return localDataSource.delete(album);
    }

    @Override
    public Single<Album> getAlbum(String param) {
        return localDataSource.getAlbum(param);
    }
}
