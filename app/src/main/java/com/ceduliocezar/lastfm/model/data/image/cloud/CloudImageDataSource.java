package com.ceduliocezar.lastfm.model.data.image.cloud;

import com.ceduliocezar.lastfm.model.data.APIService;
import com.ceduliocezar.lastfm.model.data.image.ImageDataSource;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Image;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;

public class CloudImageDataSource implements ImageDataSource {
    private APIService apiService;

    @Inject
    public CloudImageDataSource(APIService apiService) {
        this.apiService = apiService;
    }

    @Override
    public Single<Image> getImage(String albumId) {
        return apiService.image(albumId).map(image -> new Image(image.getUrl(), albumId));
    }

    @Override
    public Completable saveAlbumImage(Image image) {
        throw new UnsupportedOperationException("Save album image not available for cloud");
    }

    @Override
    public Completable deleteImage(Album album) {
        throw new UnsupportedOperationException("Delete image not available on cloud");
    }
}
