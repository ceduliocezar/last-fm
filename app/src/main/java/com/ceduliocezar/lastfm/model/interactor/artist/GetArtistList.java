package com.ceduliocezar.lastfm.model.interactor.artist;

import com.ceduliocezar.lastfm.di.threading.ExecutionThread;
import com.ceduliocezar.lastfm.di.threading.PostExecutionThread;
import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.entity.Artist;
import com.ceduliocezar.lastfm.model.interactor.SingleUseCase;
import com.ceduliocezar.lastfm.model.repository.artist.ArtistRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;

/**
 * Use case that searches for all artists based on a search term.
 */
public class GetArtistList extends SingleUseCase<List<Artist>, String> {

    private static final String TAG = "GetArtistList";
    private ArtistRepository artistRepository;

    @Inject
    GetArtistList(PostExecutionThread postExecutionThread,
                  ExecutionThread executionThread,
                  CompositeDisposable compositeDisposable,
                  ArtistRepository artistRepository) {
        super(postExecutionThread, executionThread, compositeDisposable);
        this.artistRepository = artistRepository;
    }

    @Override
    protected Single<List<Artist>> buildSingle(String param) {
        AppLog.d(TAG, "buildSingle: " + param);
        return artistRepository.list(param);
    }
}
