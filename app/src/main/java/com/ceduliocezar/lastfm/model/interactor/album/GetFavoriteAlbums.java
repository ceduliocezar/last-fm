package com.ceduliocezar.lastfm.model.interactor.album;

import com.ceduliocezar.lastfm.di.threading.ExecutionThread;
import com.ceduliocezar.lastfm.di.threading.PostExecutionThread;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.interactor.SingleUseCase;
import com.ceduliocezar.lastfm.model.repository.album.AlbumRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;

public class GetFavoriteAlbums extends SingleUseCase<List<Album>, Void> {

    private AlbumRepository getAlbumRepository;

    @Inject
    public GetFavoriteAlbums(PostExecutionThread postExecutionThread, ExecutionThread executionThread,
                             CompositeDisposable compositeDisposable,
                             AlbumRepository getAlbumRepository) {
        super(postExecutionThread, executionThread, compositeDisposable);
        this.getAlbumRepository = getAlbumRepository;
    }

    @Override
    protected Single<List<Album>> buildSingle(Void param) {
        return getAlbumRepository.favoriteAlbums();
    }
}
