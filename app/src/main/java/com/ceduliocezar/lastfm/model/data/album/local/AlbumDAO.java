package com.ceduliocezar.lastfm.model.data.album.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.ceduliocezar.lastfm.model.entity.Album;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

@Dao
public interface AlbumDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(RoomAlbum album);

    @Query("SELECT * FROM album")
    Single<List<RoomAlbum>> favoriteAlbums();

    @Delete
    void delete(RoomAlbum album);

    @Query("SELECT * FROM album WHERE id = :albumId")
    Single<RoomAlbum> get(String albumId);
}
