package com.ceduliocezar.lastfm.model.data;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.ceduliocezar.lastfm.model.data.album.image.AlbumImageDAO;
import com.ceduliocezar.lastfm.model.data.album.image.RoomAlbumImage;
import com.ceduliocezar.lastfm.model.data.album.local.AlbumDAO;
import com.ceduliocezar.lastfm.model.data.album.local.RoomAlbum;
import com.ceduliocezar.lastfm.model.data.artist.local.ArtistDAO;
import com.ceduliocezar.lastfm.model.data.artist.local.RoomArtist;
import com.ceduliocezar.lastfm.model.data.track.local.RoomTrack;
import com.ceduliocezar.lastfm.model.data.track.local.TrackDAO;

@Database(entities = {RoomArtist.class, RoomAlbum.class, RoomAlbumImage.class, RoomTrack.class}, version = 1)
public abstract class AppDataBase extends RoomDatabase {
    public abstract ArtistDAO artistDAO();

    public abstract AlbumDAO albumDAO();

    public abstract AlbumImageDAO albumImageDAO();

    public abstract TrackDAO trackDAO();
}
