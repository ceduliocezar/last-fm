package com.ceduliocezar.lastfm.model.interactor.track;

import android.support.annotation.Nullable;

import com.ceduliocezar.lastfm.di.threading.ExecutionThread;
import com.ceduliocezar.lastfm.di.threading.PostExecutionThread;
import com.ceduliocezar.lastfm.model.entity.Track;
import com.ceduliocezar.lastfm.model.interactor.CompletableUseCase;
import com.ceduliocezar.lastfm.model.repository.track.TrackRepository;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.disposables.CompositeDisposable;

public class SaveTracks extends CompletableUseCase<SaveTracks.Request> {
    private TrackRepository trackRepository;

    @Inject
    public SaveTracks(PostExecutionThread postExecutionThread,
                      ExecutionThread executionThread,
                      CompositeDisposable compositeDisposable,
                      TrackRepository trackRepository) {
        super(postExecutionThread, executionThread, compositeDisposable);
        this.trackRepository = trackRepository;
    }

    @Override
    public Completable buildCompletable(@Nullable SaveTracks.Request request) {
        return trackRepository.saveAlbumTracks(request.getTracks(), request.getAlbumId());
    }

    public static class Request {
        private List<Track> tracks;
        private String albumId;

        public Request(List<Track> tracks, String albumId) {
            this.tracks = tracks;
            this.albumId = albumId;
        }

        public List<Track> getTracks() {
            return tracks;
        }

        public String getAlbumId() {
            return albumId;
        }
    }
}
