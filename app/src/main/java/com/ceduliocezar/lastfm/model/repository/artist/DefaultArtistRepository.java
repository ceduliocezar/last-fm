package com.ceduliocezar.lastfm.model.repository.artist;


import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.data.artist.ArtistDataSource;
import com.ceduliocezar.lastfm.model.entity.Artist;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * <p>Default implementation of {@link ArtistRepository}. This class is in charge of deciding
 * if it will get data from local or cloud data source.</p>
 * <p><b>Only cloud available until now.</b></p>
 */
public class DefaultArtistRepository implements ArtistRepository {
    private static final String TAG = "DefaultArtistRepository";

    private ArtistDataSource cloudDataSource;
    private ArtistDataSource localDataSource;

    @Inject
    public DefaultArtistRepository(@Named("cloudDataSource") ArtistDataSource cloudDataSource,
                            @Named("localDataSource") ArtistDataSource localDataSource) {
        this.cloudDataSource = cloudDataSource;
        this.localDataSource = localDataSource;
    }

    @Override
    public Single<List<Artist>> list(String searchTerm) {
        AppLog.d(TAG, "artists: " + searchTerm);
        return cloudDataSource.list(searchTerm);
    }

    @Override
    public Completable save(Artist artist) {
        return localDataSource.save(artist);
    }

    @Override
    public Single<Artist> get(String artistId) {
        return localDataSource
                .getArtist(artistId)
                .onErrorResumeNext(throwable -> cloudDataSource.getArtist(artistId));
    }
}
