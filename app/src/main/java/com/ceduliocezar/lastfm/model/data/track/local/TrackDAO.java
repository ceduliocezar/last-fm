package com.ceduliocezar.lastfm.model.data.track.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

@Dao
public interface TrackDAO {

    @Query("SELECT * FROM track WHERE album_id = :albumId")
    Single<List<RoomTrack>> list(String albumId);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void save(List<RoomTrack> tracks);

    @Query("DELETE FROM track WHERE album_id = :albumId")
    void delete(String albumId);
}
