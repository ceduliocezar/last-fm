package com.ceduliocezar.lastfm.model.data.album.local;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.ceduliocezar.lastfm.model.data.artist.local.RoomArtist;

import java.io.Serializable;

import static android.arch.persistence.room.ForeignKey.CASCADE;
import static android.arch.persistence.room.ForeignKey.NO_ACTION;

@Entity(tableName = "album",
        foreignKeys = @ForeignKey(entity = RoomArtist.class,
                parentColumns = "id",
                childColumns = "artist_id",
                onDelete = CASCADE),
        indices = @Index("artist_id"))
public class RoomAlbum implements Serializable {
    @PrimaryKey
    @NonNull
    private String id;
    private String name;
    @ColumnInfo(name = "artist_id")
    private String artistId;

    public RoomAlbum(String id,
                     String name,
                     String artistId) {
        this.id = id;
        this.name = name;
        this.artistId = artistId;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getArtistId() {
        return artistId;
    }

    @Override
    public String toString() {
        return "RoomAlbum{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", artistId='" + artistId + '\'' +
                '}';
    }
}