package com.ceduliocezar.lastfm.model.data.track.local;

import com.ceduliocezar.lastfm.model.data.AppDataBase;
import com.ceduliocezar.lastfm.model.data.track.TrackDataSource;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Track;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;

public class LocalTrackDataSource implements TrackDataSource {

    private AppDataBase appDataBase;

    @Inject
    public LocalTrackDataSource(AppDataBase appDataBase) {
        this.appDataBase = appDataBase;
    }

    @Override
    public Single<List<Track>> list(String albumId) {
        return appDataBase.trackDAO()
                .list(albumId)
                .map(this::mapToTrack);
    }

    private List<Track> mapToTrack(List<RoomTrack> roomTracks) {
        List<Track> tracks = new ArrayList<>();

        for (RoomTrack roomTrack : roomTracks) {
            tracks.add(new Track(roomTrack.getName()));
        }
        return tracks;
    }

    @Override
    public Completable save(List<Track> tracks, String albumId) {
        return Completable.create(emitter -> {
            appDataBase.trackDAO().save(mapToRoom(tracks, albumId));
            emitter.onComplete();
        });
    }

    @Override
    public Completable deleteTracks(Album album) {
        return Completable.create(emitter -> {
            appDataBase.trackDAO().delete(album.getId());
            emitter.onComplete();
        });
    }

    private List<RoomTrack> mapToRoom(List<Track> tracks, String albumId) {
        List<RoomTrack> roomTracks = new ArrayList<>();

        for (Track track : tracks) {
            roomTracks.add(new RoomTrack(track.getName(), albumId));
        }

        return roomTracks;
    }
}
