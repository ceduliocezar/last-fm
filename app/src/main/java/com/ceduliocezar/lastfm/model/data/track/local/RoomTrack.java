package com.ceduliocezar.lastfm.model.data.track.local;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;

import com.ceduliocezar.lastfm.model.data.album.local.RoomAlbum;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "track",
        foreignKeys = @ForeignKey(entity = RoomAlbum.class,
                parentColumns = "id",
                childColumns = "album_id",
                onDelete = CASCADE),
        indices = @Index("album_id"))
public class RoomTrack {
    @PrimaryKey(autoGenerate = true)
    private Long id;
    private String name;

    @ColumnInfo(name = "album_id")
    private String albumId;

    @SuppressWarnings("unused") // room constructor
    public RoomTrack(Long id, String name, String albumId) {
        this.id = id;
        this.name = name;
        this.albumId = albumId;
    }

    @Ignore
    RoomTrack(String name, String albumId) {
        this.name = name;
        this.albumId = albumId;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAlbumId() {
        return albumId;
    }

    public void setAlbumId(String albumId) {
        this.albumId = albumId;
    }

    @Override
    public String toString() {
        return "RoomTrack{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", albumId='" + albumId + '\'' +
                '}';
    }
}
