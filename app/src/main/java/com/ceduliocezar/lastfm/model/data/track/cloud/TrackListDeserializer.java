package com.ceduliocezar.lastfm.model.data.track.cloud;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class TrackListDeserializer implements JsonDeserializer<List<Track>> {

    @Inject
    TrackListDeserializer() {
        // mandatory di constructor
    }

    @Override
    public List<Track> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext ctx) {
        List<Track> tracks = new ArrayList<>();

        JsonArray jsonArray = json.getAsJsonObject()
                .get("album").getAsJsonObject()
                .get("tracks").getAsJsonObject()
                .get("track")
                .getAsJsonArray();

        for (JsonElement e : jsonArray) {
            tracks.add(ctx.deserialize(e, Track.class));
        }

        return tracks;
    }
}
