package com.ceduliocezar.lastfm.model.data.track;

import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Track;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface TrackDataSource {

    Single<List<Track>> list(String albumId);

    Completable save(List<Track> tracks, String albumId);

    Completable deleteTracks(Album album);
}
