package com.ceduliocezar.lastfm.model.data;

import com.ceduliocezar.lastfm.model.data.album.cloud.Album;
import com.ceduliocezar.lastfm.model.data.artist.cloud.Artist;
import com.ceduliocezar.lastfm.model.data.artist.cloud.ArtistTO;
import com.ceduliocezar.lastfm.model.data.image.cloud.Image;
import com.ceduliocezar.lastfm.model.data.track.cloud.Track;

import java.util.List;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIService {

    @GET("?method=artist.search")
    Single<List<Artist>> artists(@Query("artist") String searchTerm);

    @GET("?method=artist.gettopalbums")
    Single<List<Album>> topAlbums(@Query("mbid") String artistId);

    @GET("?method=album.getinfo")
    Single<List<Track>> tracks(@Query("mbid") String albumId);

    @GET("?method=artist.getinfo")
    Single<ArtistTO> artist(@Query("mbid") String artistId);

    @GET("?method=album.getinfo")
    Single<Image> image(@Query("mbid") String albumId);
}
