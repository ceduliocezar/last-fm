package com.ceduliocezar.lastfm.model.interactor.artist;

import android.support.annotation.Nullable;

import com.ceduliocezar.lastfm.di.threading.ExecutionThread;
import com.ceduliocezar.lastfm.di.threading.PostExecutionThread;
import com.ceduliocezar.lastfm.model.entity.Artist;
import com.ceduliocezar.lastfm.model.interactor.CompletableUseCase;
import com.ceduliocezar.lastfm.model.repository.artist.ArtistRepository;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.disposables.CompositeDisposable;

public class SaveArtist extends CompletableUseCase<Artist> {

    private ArtistRepository artistRepository;

    @Inject
    public SaveArtist(PostExecutionThread postExecutionThread,
                      ExecutionThread executionThread,
                      CompositeDisposable compositeDisposable,
                      ArtistRepository artistRepository) {
        super(postExecutionThread, executionThread, compositeDisposable);
        this.artistRepository = artistRepository;
    }

    @Override
    public Completable buildCompletable(@Nullable Artist artist) {
        return artistRepository.save(artist);
    }
}
