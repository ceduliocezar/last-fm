package com.ceduliocezar.lastfm.model.data.album.cloud;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.data.APIService;
import com.ceduliocezar.lastfm.model.data.album.AlbumDataSource;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Implementation of {@link com.ceduliocezar.lastfm.model.data.album.AlbumDataSource} that lookup for data on LastFM API.
 */
public class CloudAlbumDataSource implements AlbumDataSource {

    private static final String TAG = "CloudAlbumDataSource";
    private APIService service;

    @Inject
    CloudAlbumDataSource(APIService service) {
        this.service = service;
    }

    @Override
    public Single<List<com.ceduliocezar.lastfm.model.entity.Album>> topAlbums(String artistId) {
        AppLog.d(TAG, "topAlbums: " + artistId);
        return service.topAlbums(artistId).map(this::mapToEntity);
    }

    private List<com.ceduliocezar.lastfm.model.entity.Album> mapToEntity(List<Album> albums) {
        List<com.ceduliocezar.lastfm.model.entity.Album> albumsEntity = new ArrayList<>();
        for (Album album : albums) {
            albumsEntity.add(new com.ceduliocezar.lastfm.model.entity.Album(album.getId(), album.getName(), album.getArtist().getId()));
        }
        return albumsEntity;
    }

    @Override
    public Completable save(com.ceduliocezar.lastfm.model.entity.Album album) {
        throw new UnsupportedOperationException("Save operation not available for cloud.");
    }

    @Override
    public Single<List<com.ceduliocezar.lastfm.model.entity.Album>> favoriteAlbums() {
        throw new UnsupportedOperationException("Favorite list not available for cloud");
    }

    @Override
    public Completable delete(com.ceduliocezar.lastfm.model.entity.Album album) {
        throw new UnsupportedOperationException("Delete not available for cloud");
    }

    @Override
    public Single<com.ceduliocezar.lastfm.model.entity.Album> getAlbum(String albumId) {
        throw new UnsupportedOperationException("Get not available for cloud");
    }
}
