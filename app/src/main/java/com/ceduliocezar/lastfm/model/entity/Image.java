package com.ceduliocezar.lastfm.model.entity;

public class Image {

    private String url;
    private String id;

    public Image(String url, String id) {
        this.url = url;
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        return "Image{" +
                "url='" + url + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}
