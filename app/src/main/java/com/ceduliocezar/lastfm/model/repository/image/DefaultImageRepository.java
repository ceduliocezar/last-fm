package com.ceduliocezar.lastfm.model.repository.image;

import com.ceduliocezar.lastfm.model.data.image.ImageDataSource;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Image;

import javax.inject.Inject;
import javax.inject.Named;

import io.reactivex.Completable;
import io.reactivex.Single;

public class DefaultImageRepository implements ImageRepository {

    private ImageDataSource localImageDataSource;
    private ImageDataSource cloudImageDataSource;

    @Inject
    DefaultImageRepository(@Named("localDataSource") ImageDataSource localImageDataSource,
                           @Named("cloudDataSource") ImageDataSource cloudImageDataSource) {
        this.localImageDataSource = localImageDataSource;
        this.cloudImageDataSource = cloudImageDataSource;
    }

    @Override
    public Single<Image> getAlbumImage(String albumId) {
        return localImageDataSource.getImage(albumId)
                .onErrorResumeNext(cloudImageDataSource.getImage(albumId));
    }

    @Override
    public Completable saveAlbumImage(Image image) {
        return localImageDataSource.saveAlbumImage(image);
    }

    @Override
    public Completable deleteAlbumImage(Album album) {
        return localImageDataSource.deleteImage(album);
    }
}
