package com.ceduliocezar.lastfm.model.interactor.album;

import android.support.annotation.Nullable;

import com.ceduliocezar.lastfm.di.threading.ExecutionThread;
import com.ceduliocezar.lastfm.di.threading.PostExecutionThread;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.interactor.CompletableUseCase;
import com.ceduliocezar.lastfm.model.repository.album.AlbumRepository;
import com.ceduliocezar.lastfm.model.repository.image.ImageRepository;
import com.ceduliocezar.lastfm.model.repository.track.TrackRepository;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.disposables.CompositeDisposable;

public class DeleteAlbum extends CompletableUseCase<Album> {

    private AlbumRepository albumRepository;
    private TrackRepository trackRepository;
    private ImageRepository imageRepository;

    @Inject
    public DeleteAlbum(PostExecutionThread postExecutionThread,
                       ExecutionThread executionThread,
                       CompositeDisposable compositeDisposable,
                       AlbumRepository albumRepository,
                       TrackRepository trackRepository,
                       ImageRepository imageRepository) {
        super(postExecutionThread, executionThread, compositeDisposable);
        this.albumRepository = albumRepository;
        this.trackRepository = trackRepository;
        this.imageRepository = imageRepository;
    }

    @Override
    public Completable buildCompletable(@Nullable Album param) {
        return albumRepository.delete(param)
                .andThen(trackRepository.deleteAlbumTracks(param))
                .andThen(imageRepository.deleteAlbumImage(param));
    }
}
