package com.ceduliocezar.lastfm.model.data.album;

import com.ceduliocezar.lastfm.model.entity.Album;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

public interface AlbumDataSource {
    Single<List<Album>> topAlbums(String artistId);

    Completable save(Album album);

    Single<List<Album>> favoriteAlbums();

    Completable delete(Album album);

    Single<Album> getAlbum(String albumId);
}
