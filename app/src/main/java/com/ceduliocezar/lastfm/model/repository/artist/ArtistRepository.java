package com.ceduliocezar.lastfm.model.repository.artist;

import com.ceduliocezar.lastfm.model.entity.Artist;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Repository of {@link Artist} entity.
 */
public interface ArtistRepository {

    /**
     * @param searchTerm Artist filter.
     * @return All artist that matches the searched term.
     */
    Single<List<Artist>> list(String searchTerm);

    Completable save(Artist artistId);

    Single<Artist> get(String artistId);
}
