package com.ceduliocezar.lastfm.model.data.artist.cloud;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class ArtistListDeserializer implements JsonDeserializer<List<Artist>> {

    @Inject
    ArtistListDeserializer() {
        // mandatory di constructor
    }

    public List<Artist> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext ctx) {
        List<Artist> artists = new ArrayList<>();

        JsonObject resultsElement = json.getAsJsonObject().get("results").getAsJsonObject();
        JsonObject artistMatchesElement = resultsElement.get("artistmatches").getAsJsonObject();

        JsonArray jsonArray = artistMatchesElement.get("artist").getAsJsonArray();
        for (JsonElement e : jsonArray) {
            artists.add(ctx.deserialize(e, Artist.class));
        }

        return artists;
    }
}
