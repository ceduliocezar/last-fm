package com.ceduliocezar.lastfm.model.interactor.album;

import android.support.annotation.Nullable;

import com.ceduliocezar.lastfm.di.threading.ExecutionThread;
import com.ceduliocezar.lastfm.di.threading.PostExecutionThread;
import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Artist;
import com.ceduliocezar.lastfm.model.entity.Image;
import com.ceduliocezar.lastfm.model.entity.Track;
import com.ceduliocezar.lastfm.model.interactor.CompletableUseCase;
import com.ceduliocezar.lastfm.model.interactor.artist.SaveArtist;
import com.ceduliocezar.lastfm.model.interactor.image.SaveAlbumImage;
import com.ceduliocezar.lastfm.model.interactor.track.SaveTracks;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.disposables.CompositeDisposable;

public class SaveAlbumAsFavorite extends CompletableUseCase<SaveAlbumAsFavorite.Request> {

    private static final String TAG = "SaveAlbumAsFavorite";
    private SaveArtist saveArtist;
    private SaveAlbum saveAlbum;
    private SaveTracks saveTracks;
    private SaveAlbumImage saveAlbumImage;

    @Inject
    SaveAlbumAsFavorite(PostExecutionThread postExecutionThread,
                        ExecutionThread executionThread,
                        CompositeDisposable compositeDisposable,
                        SaveAlbum saveAlbum,
                        SaveTracks saveTracks,
                        SaveArtist saveArtist,
                        SaveAlbumImage saveAlbumImage) {
        super(postExecutionThread, executionThread, compositeDisposable);
        this.saveAlbum = saveAlbum;
        this.saveTracks = saveTracks;
        this.saveArtist = saveArtist;
        this.saveAlbumImage = saveAlbumImage;
    }

    @Override
    public Completable buildCompletable(@Nullable SaveAlbumAsFavorite.Request request) {
        AppLog.d(TAG, "buildCompletable: " + request);

        Artist artist = request.getArtist();
        Album album = request.getAlbum();
        Image image = request.getImage();
        List<Track> tracks = request.getTracks();

        return saveArtist.buildCompletable(artist)
                .andThen(saveAlbum.buildCompletable(album))
                .andThen(saveTracks.buildCompletable(new SaveTracks.Request(tracks, album.getId()))
                        .andThen(saveAlbumImage.buildCompletable(image)));
    }

    public static class Request {
        private Artist artist;
        private Album album;
        private Image image;
        private List<Track> tracks;

        public Request(Artist artist,
                       Album album,
                       Image image,
                       List<Track> tracks) {
            this.artist = artist;
            this.album = album;
            this.image = image;
            this.tracks = tracks;
        }

        public Artist getArtist() {
            return artist;
        }

        public Album getAlbum() {
            return album;
        }

        public Image getImage() {
            return image;
        }

        public List<Track> getTracks() {
            return tracks;
        }

        @Override
        public String toString() {
            return "Request{" +
                    "artist=" + artist +
                    ", album=" + album +
                    ", image=" + image +
                    ", tracks=" + tracks +
                    '}';
        }
    }
}
