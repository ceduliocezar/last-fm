package com.ceduliocezar.lastfm.model.interactor;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.ceduliocezar.lastfm.di.threading.ExecutionThread;
import com.ceduliocezar.lastfm.di.threading.PostExecutionThread;
import com.ceduliocezar.lastfm.logging.AppLog;

import io.reactivex.Completable;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableCompletableObserver;

public abstract class CompletableUseCase<P> {

    private static final String TAG = "SingleUseCase";
    private PostExecutionThread postExecutionThread;
    private ExecutionThread executionThread;
    private CompositeDisposable compositeDisposable;


    public CompletableUseCase(PostExecutionThread postExecutionThread,
                              ExecutionThread executionThread,
                              CompositeDisposable compositeDisposable) {
        this.postExecutionThread = postExecutionThread;
        this.executionThread = executionThread;
        this.compositeDisposable = compositeDisposable;
    }

    /**
     * Executes the use case.
     *
     * @param param    Uses type <b>P</b> as param to create a completable.
     * @param observer Observes the result.
     */
    public void execute(@Nullable P param, @NonNull DisposableCompletableObserver observer) {
        AppLog.d(TAG, String.format("execute: param:%s", String.valueOf(param)));

        prepareCompletableForSubscribe(param).subscribe(observer);

        compositeDisposable.add(observer);
    }

    public Completable prepareCompletableForSubscribe(@Nullable P param) {
        return buildCompletable(param)
                .subscribeOn(executionThread.getScheduler())
                .observeOn(postExecutionThread.getScheduler());
    }

    public abstract Completable buildCompletable(@Nullable P param);


    public void dispose() {
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }
}