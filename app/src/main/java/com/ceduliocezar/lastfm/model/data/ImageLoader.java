package com.ceduliocezar.lastfm.model.data;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.net.URLConnection;

import javax.inject.Inject;

public class ImageLoader {

    @Inject
    public ImageLoader() {
        // mandatory di constructor
    }

    public void downloadFile(String urlString, File outputFile) throws IOException {
        URL url = new URL(urlString);
        URLConnection connection = url.openConnection();
        int contentLength = connection.getContentLength();

        DataInputStream stream = new DataInputStream(url.openStream());

        byte[] buffer = new byte[contentLength];
        stream.readFully(buffer);
        stream.close();

        DataOutputStream fos = new DataOutputStream(new FileOutputStream(outputFile));
        fos.write(buffer);
        fos.flush();
        fos.close();
    }
}
