package com.ceduliocezar.lastfm.model.data.artist.cloud;

import android.support.annotation.NonNull;

import com.ceduliocezar.lastfm.model.data.APIService;
import com.ceduliocezar.lastfm.model.data.artist.ArtistDataSource;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;

/**
 * Implementation of {@link ArtistDataSource} that lookup for data on LastFM API.
 */
public class CloudArtistDataSource implements ArtistDataSource {

    private APIService service;

    @Inject
    CloudArtistDataSource(APIService service) {
        this.service = service;
    }

    @Override
    public Single<List<com.ceduliocezar.lastfm.model.entity.Artist>> list(String searchTerm) {
        return service.artists(searchTerm).map(this::mapListToEntity);
    }

    @Override
    public Completable save(com.ceduliocezar.lastfm.model.entity.Artist artist) {
        throw new UnsupportedOperationException("Save not available for cloud");
    }

    @Override
    public Single<com.ceduliocezar.lastfm.model.entity.Artist> getArtist(String artistId) {
        return service.artist(artistId).map(artistTO -> {
            Artist artist = artistTO.getArtist();
            return new com.ceduliocezar.lastfm.model.entity.Artist(artist.getId(), artist.getName());
        });
    }

    private List<com.ceduliocezar.lastfm.model.entity.Artist> mapListToEntity(List<Artist> artists) {
        List<com.ceduliocezar.lastfm.model.entity.Artist> entityArtists = new ArrayList<>();

        for (Artist artist : artists) {
            entityArtists.add(mapToEntity(artist));
        }
        return entityArtists;
    }

    @NonNull
    private com.ceduliocezar.lastfm.model.entity.Artist mapToEntity(Artist artist) {
        return new com.ceduliocezar.lastfm.model.entity.Artist(artist.getId(), artist.getName());
    }
}
