package com.ceduliocezar.lastfm.model.data.artist.local;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import io.reactivex.Single;

@Dao
public interface ArtistDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(RoomArtist artist);

    @Query("DELETE FROM artist where id = :id")
    void delete(String id);

    @Query("SELECT * FROM artist where id = :artistId")
    Single<RoomArtist> get(String artistId);
}
