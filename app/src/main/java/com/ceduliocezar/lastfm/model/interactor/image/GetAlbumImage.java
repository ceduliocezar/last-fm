package com.ceduliocezar.lastfm.model.interactor.image;

import com.ceduliocezar.lastfm.di.threading.ExecutionThread;
import com.ceduliocezar.lastfm.di.threading.PostExecutionThread;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Image;
import com.ceduliocezar.lastfm.model.interactor.SingleUseCase;
import com.ceduliocezar.lastfm.model.repository.image.ImageRepository;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;

public class GetAlbumImage extends SingleUseCase<Image, Album> {

    private ImageRepository imageRepository;

    @Inject
    GetAlbumImage(PostExecutionThread postExecutionThread,
                  ExecutionThread executionThread,
                  CompositeDisposable compositeDisposable,
                  ImageRepository imageRepository) {
        super(postExecutionThread, executionThread, compositeDisposable);
        this.imageRepository = imageRepository;
    }

    @Override
    protected Single<Image> buildSingle(Album param) {
        return imageRepository.getAlbumImage(param.getId());
    }
}
