package com.ceduliocezar.lastfm.model.data.image.cloud;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.lang.reflect.Type;

import javax.inject.Inject;

public class ImageListDeserializer implements JsonDeserializer<Image> {

    @Inject
    ImageListDeserializer() {
        // mandatory di constructor
    }

    public Image deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext ctx) {
        JsonArray jsonArray = json.getAsJsonObject()
                .get("album").getAsJsonObject()
                .get("image").getAsJsonArray();

        JsonObject jsonObject = jsonArray.get(jsonArray.size() - 1).getAsJsonObject();

        return new Image(jsonObject.get("#text").getAsString(), jsonObject.get("size").getAsString());
    }
}
