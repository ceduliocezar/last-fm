package com.ceduliocezar.lastfm.model.data.album.cloud;

import com.google.gson.JsonArray;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class AlbumListDeserializer implements JsonDeserializer<List<Album>> {

    @Inject
    AlbumListDeserializer() {
        // mandatory di constructor
    }

    @Override
    public List<Album> deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext ctx) {
        List<Album> albums = new ArrayList<>();

        JsonObject topAlbumsObject = json.getAsJsonObject().get("topalbums").getAsJsonObject();

        JsonArray jsonArray = topAlbumsObject.get("album").getAsJsonArray();
        for (JsonElement e : jsonArray) {
            albums.add(ctx.deserialize(e, Album.class));
        }

        return albums;
    }
}
