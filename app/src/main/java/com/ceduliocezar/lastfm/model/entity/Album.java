package com.ceduliocezar.lastfm.model.entity;

import java.io.Serializable;

public class Album implements Serializable {
    private String id;
    private String name;
    private String artistId;


    public Album(String id, String name, String artistId) {
        this.id = id;
        this.name = name;
        this.artistId = artistId;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getArtistId() {
        return artistId;
    }

    @Override
    public String toString() {
        return "Album{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", artistId='" + artistId + '\'' +
                '}';
    }


}
