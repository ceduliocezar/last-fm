package com.ceduliocezar.lastfm.model.data.artist.cloud;

import com.google.gson.annotations.SerializedName;

public class ArtistTO {

    @SerializedName("artist")
    private Artist artist;

    public ArtistTO(Artist artist) {
        this.artist = artist;
    }

    public Artist getArtist() {
        return artist;
    }

    @Override
    public String toString() {
        return "ArtistTO{" +
                "artist=" + artist +
                '}';
    }
}
