package com.ceduliocezar.lastfm.model.data.album.image;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;
import android.arch.persistence.room.Index;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.ceduliocezar.lastfm.model.data.album.local.RoomAlbum;

import static android.arch.persistence.room.ForeignKey.CASCADE;

@Entity(tableName = "album_image",
        foreignKeys = @ForeignKey(entity = RoomAlbum.class,
                parentColumns = "id",
                childColumns = "id",
                onDelete = CASCADE),
        indices = @Index("id"))
public class RoomAlbumImage {
    @PrimaryKey
    @NonNull
    private String id;
    private String url;

    public RoomAlbumImage(String url, String id) {
        this.url = url;
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "RoomAlbumImage{" +
                "url='" + url + '\'' +
                ", id='" + id + '\'' +
                '}';
    }
}