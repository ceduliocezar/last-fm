package com.ceduliocezar.lastfm.model.data.album.local;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.data.AppDataBase;
import com.ceduliocezar.lastfm.model.data.album.AlbumDataSource;
import com.ceduliocezar.lastfm.model.entity.Album;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;

public class LocalAlbumDataSource implements AlbumDataSource {
    private static final String TAG = "LocalAlbumDataSource";

    private AppDataBase appDataBase;

    @Inject
    public LocalAlbumDataSource(AppDataBase appDataBase) {
        this.appDataBase = appDataBase;
    }

    @Override
    public Single<List<Album>> topAlbums(String artistId) {
        AppLog.d(TAG, "topAlbums: " + artistId);
        throw new UnsupportedOperationException("top albums not available locally");
    }

    @Override
    public Completable save(Album album) {
        AppLog.d(TAG, "save: album save");
        return Completable.create(emitter -> {
            appDataBase.albumDAO().insert(mapToRoom(album));
            emitter.onComplete();
        });
    }

    @Override
    public Single<List<Album>> favoriteAlbums() {
        return appDataBase.albumDAO()
                .favoriteAlbums()
                .map(this::mapEntity);
    }

    @Override
    public Completable delete(Album album) {
        return Completable.create(emitter -> {
            appDataBase.albumDAO().delete(mapToRoom(album));
            emitter.onComplete();
        });
    }

    @Override
    public Single<Album> getAlbum(String albumId) {
        return appDataBase.albumDAO()
                .get(albumId)
                .map(roomAlbum -> new Album(roomAlbum.getId(), roomAlbum.getName(), roomAlbum.getArtistId()));
    }

    private List<Album> mapEntity(List<RoomAlbum> albums) {

        List<Album> albumsEntity = new ArrayList<>();
        for (RoomAlbum album : albums) {
            albumsEntity.add(new Album(album.getId(), album.getName(), album.getArtistId()));
        }
        return albumsEntity;
    }

    private RoomAlbum mapToRoom(Album album) {
        return new RoomAlbum(album.getId(), album.getName(), album.getArtistId());
    }
}
