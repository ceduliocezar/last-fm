package com.ceduliocezar.lastfm.model.repository.image;


import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Image;

import io.reactivex.Completable;
import io.reactivex.CompletableSource;
import io.reactivex.Single;

public interface ImageRepository {

    Single<Image> getAlbumImage(String albumId);

    Completable saveAlbumImage(Image image);

    Completable deleteAlbumImage(Album album);
}
