package com.ceduliocezar.lastfm.model.interactor.artist;

import com.ceduliocezar.lastfm.di.threading.ExecutionThread;
import com.ceduliocezar.lastfm.di.threading.PostExecutionThread;
import com.ceduliocezar.lastfm.model.entity.Artist;
import com.ceduliocezar.lastfm.model.interactor.SingleUseCase;
import com.ceduliocezar.lastfm.model.repository.artist.ArtistRepository;

import javax.inject.Inject;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;

public class GetArtistById extends SingleUseCase<Artist, String>{
    private ArtistRepository artistRepository;

    @Inject
    public GetArtistById(PostExecutionThread postExecutionThread,
                         ExecutionThread executionThread,
                         CompositeDisposable compositeDisposable,
                         ArtistRepository artistRepository) {
        super(postExecutionThread, executionThread, compositeDisposable);
        this.artistRepository = artistRepository;
    }

    @Override
    protected Single<Artist> buildSingle(String param) {
        return artistRepository.get(param);
    }
}
