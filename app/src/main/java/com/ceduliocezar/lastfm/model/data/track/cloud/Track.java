package com.ceduliocezar.lastfm.model.data.track.cloud;

public class Track {
    private String name;

    public Track(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Track{" +
                "name='" + name + '\'' +
                '}';
    }
}
