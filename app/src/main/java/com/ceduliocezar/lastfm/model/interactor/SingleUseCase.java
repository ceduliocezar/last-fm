package com.ceduliocezar.lastfm.model.interactor;

import com.ceduliocezar.lastfm.di.threading.ExecutionThread;
import com.ceduliocezar.lastfm.di.threading.PostExecutionThread;
import com.ceduliocezar.lastfm.logging.AppLog;

import io.reactivex.Single;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableSingleObserver;

/**
 * Abstraction for use cases that returns singles
 *
 * @param <R> Return type.
 * @param <P> Param type.
 */
public abstract class SingleUseCase<R, P> {

    private static final String TAG = "SingleUseCase";
    private PostExecutionThread postExecutionThread;
    private ExecutionThread executionThread;
    private CompositeDisposable compositeDisposable;


    public SingleUseCase(PostExecutionThread postExecutionThread,
                         ExecutionThread executionThread,
                         CompositeDisposable compositeDisposable) {
        this.postExecutionThread = postExecutionThread;
        this.executionThread = executionThread;
        this.compositeDisposable = compositeDisposable;
    }

    /**
     * Executes the use case.
     *
     * @param param    Uses type <b>P</b> as param to create the single.
     * @param observer Observes the result of type <b>R</b>.
     */
    public void execute(P param, DisposableSingleObserver<R> observer) {
        AppLog.d(TAG, String.format("execute: param:%s", param));

        buildSingle(param)
                .subscribeOn(executionThread.getScheduler())
                .observeOn(postExecutionThread.getScheduler())
                .subscribe(observer);

        compositeDisposable.add(observer);
    }

    protected abstract Single<R> buildSingle(P param);


    public void dispose() {
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }
}
