package com.ceduliocezar.lastfm.model.data.artist.local;

import com.ceduliocezar.lastfm.model.data.AppDataBase;
import com.ceduliocezar.lastfm.model.data.artist.ArtistDataSource;
import com.ceduliocezar.lastfm.model.entity.Artist;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Completable;
import io.reactivex.Single;

public class LocalArtistDataSource implements ArtistDataSource {
    private AppDataBase appDataBase;

    @Inject
    public LocalArtistDataSource(AppDataBase appDataBase) {
        this.appDataBase = appDataBase;
    }

    @Override
    public Single<List<Artist>> list(String searchTerm) {
        throw new UnsupportedOperationException("List not available locally");
    }

    @Override
    public Completable save(Artist artist) {
        return Completable.create(emitter -> {
            appDataBase.artistDAO().insert(mapToRoomEntity(artist));
            emitter.onComplete();
        });
    }

    @Override
    public Single<Artist> getArtist(String artistId) {
        return appDataBase.artistDAO()
                .get(artistId)
                .map(this::mapToEntity);
    }

    private Artist mapToEntity(RoomArtist roomArtist) {
        return new Artist(roomArtist.getId(), roomArtist.getName());
    }

    private RoomArtist mapToRoomEntity(Artist artist) {
        return new RoomArtist(artist.getId(), artist.getName());
    }
}
