package com.ceduliocezar.lastfm.di.threading;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Scheduler;
import io.reactivex.schedulers.Schedulers;

@Singleton
public class BackgroundThread implements ExecutionThread {

    @Inject
    BackgroundThread() {
        // mandatory di constructor
    }

    @Override
    public Scheduler getScheduler() {
        return Schedulers.newThread();
    }
}
