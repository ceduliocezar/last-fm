package com.ceduliocezar.lastfm.di.threading;

import io.reactivex.Scheduler;

public interface ExecutionThread {
    Scheduler getScheduler();
}
