package com.ceduliocezar.lastfm.di.module;

import com.ceduliocezar.lastfm.presentation.album.detail.AlbumDetailContract;
import com.ceduliocezar.lastfm.presentation.album.detail.AlbumDetailPresenter;
import com.ceduliocezar.lastfm.presentation.album.favorite.FavoriteAlbumContract;
import com.ceduliocezar.lastfm.presentation.album.favorite.FavoriteAlbumPresenter;
import com.ceduliocezar.lastfm.presentation.album.top.TopAlbumContract;
import com.ceduliocezar.lastfm.presentation.album.top.TopAlbumPresenter;
import com.ceduliocezar.lastfm.presentation.artist.search.ArtistSearchContract;
import com.ceduliocezar.lastfm.presentation.artist.search.ArtistSearchPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class PresentationModule {

    @Provides
    ArtistSearchContract.Presenter providesArtistSearchContract(ArtistSearchPresenter artistSearchPresenter) {
        return artistSearchPresenter;
    }

    @Provides
    TopAlbumContract.Presenter providesTopAlbumPresenter(TopAlbumPresenter topAlbumPresenter) {
        return topAlbumPresenter;
    }

    @Provides
    AlbumDetailContract.Presenter providesAlbumDetailPresenter(AlbumDetailPresenter albumDetailPresenter) {
        return albumDetailPresenter;
    }

    @Provides
    FavoriteAlbumContract.Presenter providesFavoriteAlbumPresenter(FavoriteAlbumPresenter favoriteAlbumPresenter) {
        return favoriteAlbumPresenter;
    }

}
