package com.ceduliocezar.lastfm.di.module;

import android.app.Application;
import android.content.Context;
import android.support.test.espresso.idling.CountingIdlingResource;

import com.ceduliocezar.lastfm.di.threading.BackgroundThread;
import com.ceduliocezar.lastfm.di.threading.ExecutionThread;
import com.ceduliocezar.lastfm.di.threading.MainThread;
import com.ceduliocezar.lastfm.di.threading.PostExecutionThread;
import com.ceduliocezar.lastfm.presentation.AppNavigator;
import com.ceduliocezar.lastfm.presentation.Navigator;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class AppModule {

    private Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @Named("applicationContext")
    @SuppressWarnings("unused")
    Context providesApplication() {
        return application;
    }

    @Singleton
    @Provides
    ExecutionThread providesExecutionThread(BackgroundThread backgroundThread) {
        return backgroundThread;
    }

    @Singleton
    @Provides
    PostExecutionThread providesPostExecutionThread(MainThread mainThread) {
        return mainThread;
    }

    @Singleton
    @Provides
    Navigator provideNavigator(AppNavigator appNavigator) {
        return appNavigator;
    }

    @Provides
    CompositeDisposable providesCompositeDisposable() {
        return new CompositeDisposable();
    }

    @Provides
    @Singleton
    CountingIdlingResource providesCountingIdlingResource() {
        return new CountingIdlingResource("global");
    }


}
