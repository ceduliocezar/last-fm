package com.ceduliocezar.lastfm.di.threading;

import javax.inject.Inject;
import javax.inject.Singleton;

import io.reactivex.Scheduler;
import io.reactivex.android.schedulers.AndroidSchedulers;

@Singleton
public class MainThread implements PostExecutionThread {

    @Inject
    MainThread() {
        // mandatory di constructor
    }

    @Override
    public Scheduler getScheduler() {
        return AndroidSchedulers.mainThread();
    }
}
