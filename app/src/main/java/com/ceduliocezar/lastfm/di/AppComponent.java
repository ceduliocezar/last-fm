package com.ceduliocezar.lastfm.di;

import com.ceduliocezar.lastfm.CustomApplication;
import com.ceduliocezar.lastfm.di.module.AppModule;
import com.ceduliocezar.lastfm.di.module.ModelModule;
import com.ceduliocezar.lastfm.di.module.PresentationModule;
import com.ceduliocezar.lastfm.presentation.MainActivity;
import com.ceduliocezar.lastfm.presentation.album.detail.AlbumDetailActivity;
import com.ceduliocezar.lastfm.presentation.album.favorite.FavoriteAlbumFragment;
import com.ceduliocezar.lastfm.presentation.album.top.TopAlbumActivity;
import com.ceduliocezar.lastfm.presentation.artist.search.ArtistSearchActivity;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = {AppModule.class,
        PresentationModule.class,
        ModelModule.class})
public interface AppComponent {

    void inject(MainActivity mainActivity);

    void inject(ArtistSearchActivity artistSearchActivity);

    void inject(TopAlbumActivity topAlbumActivity);

    void inject(FavoriteAlbumFragment favoriteAlbumFragment);

    void inject(AlbumDetailActivity albumDetailActivity);

    void inject(CustomApplication customApplication);
}
