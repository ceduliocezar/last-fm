package com.ceduliocezar.lastfm.di.module;

import android.arch.persistence.room.Room;
import android.content.Context;

import com.ceduliocezar.lastfm.BuildConfig;
import com.ceduliocezar.lastfm.model.data.APIService;
import com.ceduliocezar.lastfm.model.data.AppDataBase;
import com.ceduliocezar.lastfm.model.data.album.AlbumDataSource;
import com.ceduliocezar.lastfm.model.data.album.cloud.Album;
import com.ceduliocezar.lastfm.model.data.album.cloud.AlbumListDeserializer;
import com.ceduliocezar.lastfm.model.data.album.cloud.CloudAlbumDataSource;
import com.ceduliocezar.lastfm.model.data.album.local.LocalAlbumDataSource;
import com.ceduliocezar.lastfm.model.data.artist.ArtistDataSource;
import com.ceduliocezar.lastfm.model.data.artist.cloud.Artist;
import com.ceduliocezar.lastfm.model.data.artist.cloud.ArtistListDeserializer;
import com.ceduliocezar.lastfm.model.data.artist.cloud.CloudArtistDataSource;
import com.ceduliocezar.lastfm.model.data.artist.local.LocalArtistDataSource;
import com.ceduliocezar.lastfm.model.data.image.ImageDataSource;
import com.ceduliocezar.lastfm.model.data.image.cloud.CloudImageDataSource;
import com.ceduliocezar.lastfm.model.data.image.cloud.Image;
import com.ceduliocezar.lastfm.model.data.image.cloud.ImageListDeserializer;
import com.ceduliocezar.lastfm.model.data.image.local.LocalImageDataSource;
import com.ceduliocezar.lastfm.model.data.track.TrackDataSource;
import com.ceduliocezar.lastfm.model.data.track.cloud.CloudTrackDataSource;
import com.ceduliocezar.lastfm.model.data.track.cloud.Track;
import com.ceduliocezar.lastfm.model.data.track.cloud.TrackListDeserializer;
import com.ceduliocezar.lastfm.model.data.track.local.LocalTrackDataSource;
import com.ceduliocezar.lastfm.model.repository.album.AlbumRepository;
import com.ceduliocezar.lastfm.model.repository.album.DefaultAlbumRepository;
import com.ceduliocezar.lastfm.model.repository.artist.ArtistRepository;
import com.ceduliocezar.lastfm.model.repository.artist.DefaultArtistRepository;
import com.ceduliocezar.lastfm.model.repository.image.DefaultImageRepository;
import com.ceduliocezar.lastfm.model.repository.image.ImageRepository;
import com.ceduliocezar.lastfm.model.repository.track.DefaultTrackRepository;
import com.ceduliocezar.lastfm.model.repository.track.TrackRepository;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.List;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class ModelModule {

    @Provides
    @Singleton
    APIService providesAPIService(@Named("apiURL") String apiURL,
                                  OkHttpClient client,
                                  RxJava2CallAdapterFactory rxJavaCallAdapterFactory,
                                  GsonConverterFactory gsonConverterFactory) {

        return new Retrofit.Builder()
                .addCallAdapterFactory(rxJavaCallAdapterFactory)
                .addConverterFactory(gsonConverterFactory)
                .client(client)
                .baseUrl(apiURL)
                .build().create(APIService.class);
    }

    @Provides
    @Singleton
    RxJava2CallAdapterFactory providesRxJavaCallAdapterFactory() {
        return RxJava2CallAdapterFactory.create();
    }

    @Provides
    @Singleton
    GsonConverterFactory providesGsonConverterFactory(ArtistListDeserializer artistListDeserializer,
                                                      AlbumListDeserializer albumListDeserializer,
                                                      TrackListDeserializer trackListDeserializer,
                                                      ImageListDeserializer imageListDeserializer) {
        Type artistArrayType = new TypeToken<List<Artist>>() {
        }.getType();

        Type albumListType = new TypeToken<List<Album>>() {
        }.getType();

        Type trackListType = new TypeToken<List<Track>>() {
        }.getType();

        Gson gson = new GsonBuilder()
                .registerTypeAdapter(artistArrayType, artistListDeserializer)
                .registerTypeAdapter(albumListType, albumListDeserializer)
                .registerTypeAdapter(trackListType, trackListDeserializer)
                .registerTypeAdapter(Image.class, imageListDeserializer)
                .create();
        return GsonConverterFactory.create(gson);
    }

    @Provides
    @Singleton
    OkHttpClient providesOkHttpClient(@Named("defaultRequestInterceptor") Interceptor interceptor) {
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.addInterceptor(interceptor);
        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(loggingInterceptor);

        return builder.build();
    }

    @Singleton
    @Provides
    @Named("defaultRequestInterceptor")
    Interceptor providesInterceptor(@Named("apiKey") String apiKey) {
        return chain -> {
            Request original = chain.request();
            HttpUrl originalHttpUrl = original.url();

            HttpUrl url = originalHttpUrl
                    .newBuilder()
                    .addQueryParameter("api_key", apiKey)
                    .addQueryParameter("format", "json")
                    .build();

            Request.Builder requestBuilder = original.newBuilder().url(url);

            Request request = requestBuilder.build();
            return chain.proceed(request);
        };
    }


    @Provides
    @Singleton
    @Named("apiURL")
    String providesApiURL() {
        return BuildConfig.API_END_POINT;
    }

    @Provides
    @Singleton
    @Named("apiKey")
    String providesApiKey() {
        return BuildConfig.API_KEY;
    }

    @Provides
    ArtistRepository providesArtistRepository(DefaultArtistRepository defaultArtistRepository) {
        return defaultArtistRepository;
    }

    @Provides
    ArtistDataSource providesArtistDataSource(CloudArtistDataSource cloudArtistDataSource) {
        return cloudArtistDataSource;
    }


    @Provides
    AlbumRepository providesAlbumRepository(DefaultAlbumRepository defaultAlbumRepository) {
        return defaultAlbumRepository;
    }

    @Provides
    @Named("cloudDataSource")
    AlbumDataSource providesCloudAlbumDataSource(CloudAlbumDataSource cloudAlbumDataSource) {
        return cloudAlbumDataSource;
    }

    @Provides
    @Named("localDataSource")
    AlbumDataSource providesLocalAlbumDataSource(LocalAlbumDataSource localAlbumDataSource) {
        return localAlbumDataSource;
    }

    @Provides
    TrackRepository providesTrackRepository(DefaultTrackRepository defaultTrackRepository) {
        return defaultTrackRepository;
    }

    @Provides
    @Singleton
    AppDataBase providesAppDatabase(@Named("applicationContext") Context context) {
        return Room.databaseBuilder(context, AppDataBase.class, "data.db").build();
    }

    @Provides
    @Named("cloudDataSource")
    ArtistDataSource providesArtistCloudDataSource(CloudArtistDataSource cloudArtistDataSource) {
        return cloudArtistDataSource;
    }

    @Provides
    @Named("localDataSource")
    ArtistDataSource providesArtistLocalDataSource(LocalArtistDataSource localArtistDataSource) {
        return localArtistDataSource;
    }

    @Provides
    @Named("localDataSource")
    TrackDataSource providesLocalTrackDataSource(LocalTrackDataSource dataSource) {
        return dataSource;
    }

    @Provides
    @Named("cloudDataSource")
    TrackDataSource providesCloudTrackDataSource(CloudTrackDataSource dataSource) {
        return dataSource;
    }

    @Provides
    ImageRepository providesImageRepository(DefaultImageRepository defaultImageRepository) {
        return defaultImageRepository;
    }

    @Provides
    @Named("cloudDataSource")
    ImageDataSource providesCloudImageDataSource(CloudImageDataSource cloudImageDataSource) {
        return cloudImageDataSource;
    }

    @Provides
    @Named("localDataSource")
    ImageDataSource providesLocalImageDataSource(LocalImageDataSource localImageDataSource) {
        return localImageDataSource;
    }

}
