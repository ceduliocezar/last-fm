package com.ceduliocezar.lastfm.presentation.album.favorite;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.ceduliocezar.lastfm.CustomApplication;
import com.ceduliocezar.lastfm.R;
import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.interactor.album.GetFavoriteAlbums;
import com.ceduliocezar.lastfm.presentation.Navigator;
import com.ceduliocezar.lastfm.presentation.album.top.AlbumAdapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.observers.DisposableSingleObserver;


public class FavoriteAlbumFragment extends Fragment implements AlbumAdapter.OnItemClickListener, FavoriteAlbumContract.View {

    private static final String TAG = "FavoriteAlbumFragment";

    @Inject
    GetFavoriteAlbums getFavoriteAlbums;

    @Inject
    FavoriteAlbumContract.Presenter presenter;

    @Inject
    Navigator navigator;

    @BindView(R.id.empty_album_text_tv)
    View emptyAlbumTextView;

    @BindView(R.id.empty_favorite_iv)
    View emptyAlbumImageView;

    @BindView(R.id.favorite_album_recycler)
    RecyclerView recyclerView;


    private FavoriteAlbumAdapter adapter;

    public FavoriteAlbumFragment() {
        // mandatory constructor
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        injectFields();
        View view = inflater.inflate(R.layout.fragment_favorite_albums, container, false);
        ButterKnife.bind(this, view);
        setupRecycler();
        return view;
    }

    private void setupRecycler() {
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        adapter = new FavoriteAlbumAdapter(new ArrayList<>(), FavoriteAlbumFragment.this);
        recyclerView.setAdapter(adapter);
    }

    private void injectFields() {
        ((CustomApplication) Objects.requireNonNull(getActivity()).getApplication()).component().inject(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateTitle();
        presenter.onViewCreated(this);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        presenter.onViewDestroy();
    }

    private void updateTitle() {
        Objects.requireNonNull(getActivity()).setTitle(getString(R.string.favorite_albums));
    }

    @Override
    public void onItemClick(Album album) {
        AppLog.d(TAG, "onItemClick: " + album);
        navigator.navigateToAlbumDetail(album, getActivity());
    }

    @Override
    public void displayAlbums(List<Album> albumList) {
        AppLog.d(TAG, "displayAlbums: " + albumList);
        adapter.addAll(albumList);
    }

    @Override
    public void displayErrorLoading() {
        AppLog.d(TAG, "displayErrorLoading: ");
    }

    @Override
    public void displayEmptyMessage() {
        emptyAlbumImageView.setVisibility(View.VISIBLE);
        emptyAlbumTextView.setVisibility(View.VISIBLE);

    }

    @Override
    public void hideEmptyMessage() {
        emptyAlbumImageView.setVisibility(View.GONE);
        emptyAlbumTextView.setVisibility(View.GONE);
    }
}

