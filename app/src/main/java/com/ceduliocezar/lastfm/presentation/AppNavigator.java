package com.ceduliocezar.lastfm.presentation;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;

import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Artist;
import com.ceduliocezar.lastfm.presentation.album.detail.AlbumDetailActivity;
import com.ceduliocezar.lastfm.presentation.album.top.TopAlbumActivity;
import com.ceduliocezar.lastfm.presentation.artist.search.ArtistSearchActivity;

import javax.inject.Inject;

/**
 * Default implementation of {@link Navigator}
 */
public class AppNavigator implements Navigator {

    @Inject
    AppNavigator() {
        // mandatory di constructor
    }

    @Override
    public void navigateToArtistSearch(Activity currentActivity, String query) {
        Intent intent = new Intent(currentActivity, ArtistSearchActivity.class);
        intent.putExtra(SearchManager.QUERY, query);
        currentActivity.startActivity(intent);
    }

    @Override
    public void navigateToArtistTopAlbums(Artist artist, Activity currentActivity) {
        Intent intent = new Intent(currentActivity, TopAlbumActivity.class);
        intent.putExtra(TopAlbumActivity.EXTRA_ARTIST, artist);
        currentActivity.startActivity(intent);
    }

    @Override
    public void navigateToAlbumDetail(Album album, Activity currentActivity) {
        Intent intent = new Intent(currentActivity, AlbumDetailActivity.class);
        intent.putExtra(AlbumDetailActivity.EXTRA_ALBUM, album);
        currentActivity.startActivity(intent);
    }
}