package com.ceduliocezar.lastfm.presentation.artist.search;

import android.support.test.espresso.idling.CountingIdlingResource;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.entity.Artist;
import com.ceduliocezar.lastfm.model.interactor.artist.GetArtistList;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableSingleObserver;

/**
 * Default implementation of @{@link ArtistSearchContract.Presenter}
 */
public class ArtistSearchPresenter implements ArtistSearchContract.Presenter {

    private static final String TAG = "ArtistSearchPresenter";
    private ArtistSearchContract.View view;
    private GetArtistList getArtistList;
    private CountingIdlingResource countingIdlingResource;

    @Inject
    public ArtistSearchPresenter(GetArtistList getArtistList,
                                 CountingIdlingResource countingIdlingResource) {
        this.getArtistList = getArtistList;
        this.countingIdlingResource = countingIdlingResource;
    }

    @Override
    public void onViewCreated(ArtistSearchContract.View view) {
        AppLog.d(TAG, "onViewCreated: " + view.toString());
        attachToView(view);
    }

    private void attachToView(ArtistSearchContract.View view) {
        this.view = view;
    }

    @Override
    public void onViewDestroyed() {
        AppLog.d(TAG, "onViewDestroyed: ");
        detachView();
        disposeUseCases();
    }

    private void disposeUseCases() {
        AppLog.d(TAG, "disposeUseCases: ");
        getArtistList.dispose();
    }

    @Override
    public void onUserSearched(String searchTerm) {
        AppLog.d(TAG, "onUserSearched: ");
        if (searchTerm.isEmpty()) {
            AppLog.i(TAG, "onUserSearched: empty input, keep the screen as it is now");
            return;
        }

        view.showLoading();
        countingIdlingResource.increment();
        getArtistList.execute(searchTerm, new DisposableSingleObserver<List<Artist>>() {
            @Override
            public void onSuccess(List<Artist> artists) {
                AppLog.d(TAG, "onSuccess: " + artists);
                view.hideLoading();
                view.showArtists(artists);
                countingIdlingResource.decrement();
            }

            @Override
            public void onError(Throwable e) {
                AppLog.e(TAG, "onError: ", e);
                view.hideLoading();
                view.warnErrorWhileLoadingArtists();
                countingIdlingResource.decrement();
            }
        });
    }

    @Override
    public void userSelected(Artist artist) {
        AppLog.d(TAG, "userSelectedAlbum: ");
        view.navigateToTopAlbums(artist);
    }

    private void detachView() {
        this.view = null;
    }

    public ArtistSearchContract.View getView() {
        return view;
    }
}
