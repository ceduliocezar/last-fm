package com.ceduliocezar.lastfm.presentation.artist.search;


import com.ceduliocezar.lastfm.model.entity.Artist;

import java.util.List;

/**
 * Defines the contract for the artist search screen.
 */
public interface ArtistSearchContract {

    /**
     * User interface part of the contract for artist search.
     */
    interface View {

        void navigateToTopAlbums(Artist artist);

        void showArtists(List<Artist> artists);

        void warnErrorWhileLoadingArtists();

        void showLoading();

        void hideLoading();
    }

    /**
     * Logic part of the contract for artist search.
     */
    interface Presenter {

        /**
         * This method is called when the view is created.
         *
         * @param view View that the user is interacting with.
         */
        void onViewCreated(View view);

        /**
         * Called then view entered on destroy state.
         */
        void onViewDestroyed();

        /**
         * Called when user typed something on the text field.
         */
        void onUserSearched(String searchTerm);

        void userSelected(Artist artist);
    }
}
