package com.ceduliocezar.lastfm.presentation.album.favorite;

import com.ceduliocezar.lastfm.model.entity.Album;

import java.util.List;

public interface FavoriteAlbumContract {

    interface View {
        void displayAlbums(List<Album> album);

        void displayErrorLoading();

        void displayEmptyMessage();

        void hideEmptyMessage();
    }

    interface Presenter {
        void onViewCreated(View view);

        void onViewDestroy();
    }
}
