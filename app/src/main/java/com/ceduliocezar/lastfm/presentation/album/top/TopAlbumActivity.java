package com.ceduliocezar.lastfm.presentation.album.top;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.ceduliocezar.lastfm.CustomApplication;
import com.ceduliocezar.lastfm.R;
import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Artist;
import com.ceduliocezar.lastfm.presentation.Navigator;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TopAlbumActivity extends AppCompatActivity implements TopAlbumContract.View {

    private static final String TAG = "TopAlbumActivity";

    public static final String EXTRA_ARTIST = "EXTRA_ARTIST";

    @BindView(R.id.top_album_container)
    View containerView;

    @BindView(R.id.top_album_recycler)
    RecyclerView recyclerView;

    @BindView(R.id.loader)
    View loader;

    @Inject
    Navigator navigator;

    @Inject
    TopAlbumContract.Presenter presenter;

    @Inject
    CountingIdlingResource countingIdlingResource;

    private AlbumAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_artist_top_album_actvity);
        ButterKnife.bind(this);
        ((CustomApplication) getApplication()).component().inject(this);

        setTitle(getGetArtistParam().getName());
        setupRecycler();

        presenter.onViewCreated(this);
    }

    private void setupRecycler() {
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new AlbumAdapter(new ArrayList<>(), album -> presenter.userSelectedAlbum(album));
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(recyclerView.getContext(), DividerItemDecoration.VERTICAL));
    }

    @Override
    public void displayAlbumResults(List<Album> albums) {
        AppLog.d(TAG, "displayAlbumResults: " + albums);
        adapter.addAll(albums);
    }

    @Override
    public void warnErrorLoading() {
        AppLog.d(TAG, "warnErrorLoading: ");
        Toast.makeText(this, R.string.error_loading_artist_album, Toast.LENGTH_SHORT).show();
    }

    @Override
    public Artist getGetArtistParam() {
        return (Artist) Objects.requireNonNull(getIntent().getExtras()).getSerializable(EXTRA_ARTIST);
    }

    @Override
    public void navigateToAlbumDetail(Album album) {
        AppLog.d(TAG, "navigateToAlbumDetail: " + album);
        navigator.navigateToAlbumDetail(album, this);
    }

    @Override
    public void notifyAlbumWasDeleted(Album album) {
        AppLog.d(TAG, "notifyAlbumWasDeleted: " + album);
        Snackbar.make(containerView,
                getString(R.string.message_album_deleted),
                Snackbar.LENGTH_SHORT)
                .show();
    }

    @Override
    public void notifyErrorDeletingAlbum(Album album) {
        AppLog.d(TAG, "notifyErrorDeletingAlbum: " + album);
        Snackbar.make(containerView,
                getString(R.string.error_message_deleting_album, album.getName()),
                Snackbar.LENGTH_SHORT)
                .show();
    }

    @Override
    public void notifyAlbumWasSaved(Album album) {
        AppLog.d(TAG, "notifyAlbumWasSaved: " + album);
        Snackbar.make(containerView,
                getString(R.string.message_album_saved),
                Snackbar.LENGTH_SHORT)
                .show();
    }

    @Override
    public void notifyErrorSavingAlbum(Album album) {
        AppLog.d(TAG, "notifyErrorSavingAlbum: " + album);
        Snackbar.make(containerView,
                getString(R.string.error_message_saving_album, album.getName()),
                Snackbar.LENGTH_SHORT)
                .show();
    }

    @Override
    public void showLoading() {
        AppLog.d(TAG, "showLoading: ");
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        AppLog.d(TAG, "hideLoading: ");
        loader.setVisibility(View.GONE);
    }
}
