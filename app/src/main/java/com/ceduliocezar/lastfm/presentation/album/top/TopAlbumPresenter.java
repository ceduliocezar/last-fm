package com.ceduliocezar.lastfm.presentation.album.top;

import android.support.test.espresso.idling.CountingIdlingResource;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Artist;
import com.ceduliocezar.lastfm.model.interactor.album.GetAlbumList;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableSingleObserver;

public class TopAlbumPresenter implements TopAlbumContract.Presenter {
    private static final String TAG = "TopAlbumPresenter";
    private TopAlbumContract.View view;
    private CountingIdlingResource countingIdlingResource;
    private GetAlbumList getAlbumList;

    @Inject
    TopAlbumPresenter(CountingIdlingResource countingIdlingResource,
                      GetAlbumList getAlbumList) {
        this.countingIdlingResource = countingIdlingResource;
        this.getAlbumList = getAlbumList;
    }

    @Override
    public void onViewCreated(TopAlbumContract.View view) {
        AppLog.d(TAG, "onViewCreated: " + view);
        attachView(view);
        loadProperties();
    }

    private void loadProperties() {
        loadAlbums();
    }

    void loadAlbums() {
        AppLog.d(TAG, "loadAlbums: ");
        countingIdlingResource.increment();
        view.showLoading();
        Artist artist = view.getGetArtistParam();
        getAlbumList.execute(artist.getId(), new DisposableSingleObserver<List<Album>>() {
            @Override
            public void onSuccess(List<Album> albums) {
                AppLog.d(TAG, "onSuccess: " + albums);
                view.displayAlbumResults(albums);
                view.hideLoading();
                countingIdlingResource.decrement();
            }

            @Override
            public void onError(Throwable e) {
                AppLog.e(TAG, "onError: ", e);
                view.hideLoading();
                view.warnErrorLoading();
                countingIdlingResource.decrement();
            }
        });
    }

    private void attachView(TopAlbumContract.View view) {
        this.view = view;
    }

    @Override
    public void onViewDestroyed() {
        AppLog.d(TAG, "onViewDestroyed: ");
        detachView();
    }

    @Override
    public void userSelectedAlbum(Album album) {
        AppLog.d(TAG, "userSelectedAlbum: " + album);
        view.navigateToAlbumDetail(album);
    }

    private void detachView() {
        AppLog.d(TAG, "detachView: ");
        this.view = null;
    }

    public TopAlbumContract.View getView() {
        return view;
    }
}
