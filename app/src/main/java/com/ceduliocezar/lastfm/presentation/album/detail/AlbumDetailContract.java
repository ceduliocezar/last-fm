package com.ceduliocezar.lastfm.presentation.album.detail;

import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Artist;
import com.ceduliocezar.lastfm.model.entity.Image;
import com.ceduliocezar.lastfm.model.entity.Track;

import java.util.List;

public interface AlbumDetailContract {


    interface View {


        void displayArtist(Artist artist);

        void displayTracks(List<Track> trackList);

        void displayAlbum(Album album);

        Album getAlbumParam();

        void notifyAlbumSaved();

        void warnErrorWhileSaving();

        void notifyAlbumRemoved();

        void warnErrorRemovingAlbum();

        void displayImage(Image image);

        void displayFavorite(Boolean favorite);
    }

    interface Presenter {

        void onViewCreated(View view);

        void onViewDestroyed();

        void userWantsToSaveAlbumAsFavorite(Album album, Artist artist, Image albumImage, List<Track> tracks);

        void userWantsToRemoveAlbumAsFavorite(Album album);
    }
}
