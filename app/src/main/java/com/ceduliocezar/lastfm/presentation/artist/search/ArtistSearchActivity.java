package com.ceduliocezar.lastfm.presentation.artist.search;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.VisibleForTesting;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.ceduliocezar.lastfm.CustomApplication;
import com.ceduliocezar.lastfm.R;
import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.entity.Artist;
import com.ceduliocezar.lastfm.presentation.Navigator;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Default implementation of {@link ArtistSearchContract.View}.
 */
public class ArtistSearchActivity extends AppCompatActivity implements ArtistSearchContract.View, SearchView.OnQueryTextListener {

    private static final String TAG = "ArtistSearchActivity";

    @Inject
    ArtistSearchContract.Presenter presenter;

    @BindView(R.id.artist_recycler)
    RecyclerView recyclerView;

    @BindView(R.id.loader)
    View loader;

    @Inject
    Navigator navigator;

    @Inject
    @VisibleForTesting
    CountingIdlingResource countingIdlingResource;

    private ArtistAdapter adapter;
    private SearchView searchView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLog.d(TAG, "onCreate: ");
        setContentView(R.layout.activity_artist_search);
        ((CustomApplication) getApplication()).component().inject(this);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setupRecycler();
        presenter.onViewCreated(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_search, menu);

        searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        searchView.setOnQueryTextListener(this);
        searchView.setQueryHint(getString(R.string.artist_search));
        searchView.setIconified(false);

        return true;
    }

    private void setupRecycler() {
        recyclerView.setHasFixedSize(true);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ArtistAdapter(new ArrayList<>(), artist -> presenter.userSelected(artist));
        recyclerView.setAdapter(adapter);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        AppLog.d(TAG, "onDestroy: ");
        presenter.onViewDestroyed();
    }

    @Override
    public void navigateToTopAlbums(Artist artist) {
        AppLog.d(TAG, "navigateToTopAlbums: ");
        navigator.navigateToArtistTopAlbums(artist, this);
    }

    @Override
    public void showArtists(List<Artist> artists) {
        AppLog.d(TAG, "showArtists: ");
        adapter.addAll(artists);
    }

    @Override
    public void warnErrorWhileLoadingArtists() {
        AppLog.d(TAG, "warnErrorWhileLoadingArtists: ");
        Toast.makeText(this, "Oops, something went wrong while loading artists.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showLoading() {
        AppLog.d(TAG, "showLoading: ");
        loader.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoading() {
        AppLog.d(TAG, "hideLoading: ");
        loader.setVisibility(View.GONE);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String s) {
        hideKeyboardFrom(this, searchView);
        presenter.onUserSearched(s);
        return true;
    }

    public static void hideKeyboardFrom(Context context, View view) {
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
        Objects.requireNonNull(inputMethodManager).hideSoftInputFromWindow(view.getWindowToken(), 0);
    }


    @Override
    public boolean onQueryTextChange(String s) {
        presenter.onUserSearched(s);
        return true;
    }
}
