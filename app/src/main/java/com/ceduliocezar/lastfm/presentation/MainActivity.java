package com.ceduliocezar.lastfm.presentation;

import android.content.Context;
import android.os.Bundle;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;

import com.ceduliocezar.lastfm.CustomApplication;
import com.ceduliocezar.lastfm.R;
import com.ceduliocezar.lastfm.logging.AppLog;

import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.UpdateManager;

import javax.inject.Inject;
import javax.inject.Named;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    @Inject
    @Named("applicationContext")
    Context context;

    @Inject
    public CountingIdlingResource countingIdlingResource;

    @Inject
    Navigator navigator;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        AppLog.d(TAG, "onCreate: ");
        setContentView(R.layout.activity_main);
        ((CustomApplication) getApplication()).component().inject(this);
        checkForUpdates();
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkForCrashes();
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterManagers();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        AppLog.d(TAG, "onCreateOptionsMenu: ");
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterManagers();
    }

    private void checkForCrashes() {
        CrashManager.register(this);
    }

    private void checkForUpdates() {
        // Remove this for store builds!
        UpdateManager.register(this);
    }

    private void unregisterManagers() {
        UpdateManager.unregister();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        AppLog.d(TAG, "onOptionsItemSelected: ");
        switch (item.getItemId()) {
            case R.id.artist_search:
                navigator.navigateToArtistSearch(this, null);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
