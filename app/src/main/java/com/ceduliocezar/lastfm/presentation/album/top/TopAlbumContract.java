package com.ceduliocezar.lastfm.presentation.album.top;

import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Artist;

import java.util.List;

public interface TopAlbumContract {

    interface View {
        void displayAlbumResults(List<Album> albums);

        void warnErrorLoading();

        Artist getGetArtistParam();

        void navigateToAlbumDetail(Album album);

        void notifyAlbumWasDeleted(Album album);

        void notifyErrorDeletingAlbum(Album album);

        void notifyAlbumWasSaved(Album album);

        void notifyErrorSavingAlbum(Album album);

        void showLoading();

        void hideLoading();
    }

    interface Presenter {
        void onViewCreated(View view);

        void onViewDestroyed();

        void userSelectedAlbum(Album album);
    }
}
