package com.ceduliocezar.lastfm.presentation.album.favorite;

import android.support.test.espresso.idling.CountingIdlingResource;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.interactor.album.GetFavoriteAlbums;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableSingleObserver;

public class FavoriteAlbumPresenter implements FavoriteAlbumContract.Presenter {
    private static final String TAG = "FavoriteAlbumPresenter";

    private GetFavoriteAlbums getFavoriteAlbums;
    private CountingIdlingResource countingIdlingResource;
    private FavoriteAlbumContract.View view;

    @Inject
    public FavoriteAlbumPresenter(GetFavoriteAlbums getFavoriteAlbums,
                                  CountingIdlingResource countingIdlingResource) {
        this.getFavoriteAlbums = getFavoriteAlbums;
        this.countingIdlingResource = countingIdlingResource;
    }

    @Override
    public void onViewCreated(FavoriteAlbumContract.View view) {
        AppLog.d(TAG, "onViewCreated: ");
        attachView(view);
        loadProperties();
    }

    private void attachView(FavoriteAlbumContract.View view) {
        this.view = view;
    }

    private void loadProperties() {
        loadAlbums();
    }

    void loadAlbums() {
        AppLog.d(TAG, "loadAlbums: ");
        countingIdlingResource.increment();
        getFavoriteAlbums.execute(null, new DisposableSingleObserver<List<Album>>() {
            @Override
            public void onSuccess(List<Album> albums) {
                AppLog.d(TAG, "onSuccess: " + albums);
                view.displayAlbums(albums);
                if (albums.isEmpty()) {
                    view.displayEmptyMessage();
                } else {
                    view.hideEmptyMessage();
                }

                countingIdlingResource.decrement();
            }

            @Override
            public void onError(Throwable e) {
                AppLog.e(TAG, "onError: ", e);
                view.displayErrorLoading();
                countingIdlingResource.decrement();
            }
        });
    }

    @Override
    public void onViewDestroy() {
        AppLog.d(TAG, "onViewDestroy: ");
        detachView();
        getFavoriteAlbums.dispose();
    }

    private void detachView() {
        this.view = null;
    }

    FavoriteAlbumContract.View getView() {
        return view;
    }
}
