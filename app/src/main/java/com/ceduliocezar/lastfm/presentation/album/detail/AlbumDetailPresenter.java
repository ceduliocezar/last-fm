package com.ceduliocezar.lastfm.presentation.album.detail;

import android.support.test.espresso.idling.CountingIdlingResource;

import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Artist;
import com.ceduliocezar.lastfm.model.entity.Image;
import com.ceduliocezar.lastfm.model.entity.Track;
import com.ceduliocezar.lastfm.model.interactor.album.DeleteAlbum;
import com.ceduliocezar.lastfm.model.interactor.album.SaveAlbumAsFavorite;
import com.ceduliocezar.lastfm.model.interactor.album.VerifyFavorite;
import com.ceduliocezar.lastfm.model.interactor.artist.GetArtistById;
import com.ceduliocezar.lastfm.model.interactor.image.GetAlbumImage;
import com.ceduliocezar.lastfm.model.interactor.track.GetAlbumTracks;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.observers.DisposableCompletableObserver;
import io.reactivex.observers.DisposableSingleObserver;

public class AlbumDetailPresenter implements AlbumDetailContract.Presenter {

    private static final String TAG = "AlbumDetailPresenter";

    private AlbumDetailContract.View view;

    private SaveAlbumAsFavorite saveAlbum;
    private DeleteAlbum deleteAlbum;
    private GetArtistById getArtistById;
    private GetAlbumTracks getAlbumTracks;
    private GetAlbumImage getAlbumImage;
    private VerifyFavorite verifyFavorite;
    private CountingIdlingResource countingIdlingResource;


    @Inject
    public AlbumDetailPresenter(SaveAlbumAsFavorite saveAlbum,
                                DeleteAlbum deleteAlbum,
                                GetArtistById getArtistById,
                                GetAlbumTracks getAlbumTracks,
                                GetAlbumImage getAlbumImage,
                                VerifyFavorite verifyFavorite,
                                CountingIdlingResource countingIdlingResource) {
        this.saveAlbum = saveAlbum;
        this.deleteAlbum = deleteAlbum;
        this.getArtistById = getArtistById;
        this.getAlbumTracks = getAlbumTracks;
        this.getAlbumImage = getAlbumImage;
        this.verifyFavorite = verifyFavorite;
        this.countingIdlingResource = countingIdlingResource;
    }

    @Override
    public void onViewCreated(AlbumDetailContract.View view) {
        AppLog.d(TAG, "onViewCreated: ");
        attachView(view);
        loadProperties();
    }

    private void loadProperties() {
        Album albumParam = view.getAlbumParam();
        view.displayAlbum(albumParam);
        loadArtist(albumParam);
        loadTracks(albumParam);
        loadAlbumImage(albumParam);
        loadFavorite(albumParam);
    }

    void loadFavorite(Album albumParam) {
        AppLog.d(TAG, "loadFavorite: ");
        onStartLoad();
        verifyFavorite.execute(albumParam.getId(), new DisposableSingleObserver<Boolean>() {
            @Override
            public void onSuccess(Boolean favorite) {
                AppLog.d(TAG, "onSuccess: " + favorite);
                view.displayFavorite(favorite);
                onFinishLoad();
            }

            @Override
            public void onError(Throwable e) {
                AppLog.e(TAG, "onError: ", e);
                view.displayFavorite(false);
                onFinishLoad();
            }
        });
    }

    void loadAlbumImage(Album albumParam) {
        AppLog.d(TAG, "loadAlbumImage: " + albumParam);
        onStartLoad();
        getAlbumImage.execute(albumParam, new DisposableSingleObserver<Image>() {
            @Override
            public void onSuccess(Image image) {
                AppLog.d(TAG, "onSuccess: " + image);
                view.displayImage(image);
                onFinishLoad();
            }

            @Override
            public void onError(Throwable e) {
                AppLog.e(TAG, "onError: ", e);
                onFinishLoad();
            }
        });
    }

    void loadTracks(Album albumParam) {
        AppLog.d(TAG, "loadTracks: ");
        onStartLoad();
        getAlbumTracks.execute(albumParam.getId(), new DisposableSingleObserver<List<Track>>() {
            @Override
            public void onSuccess(List<Track> tracks) {
                AppLog.d(TAG, "onSuccess: loadTracks:" + tracks);
                view.displayTracks(tracks);
                onFinishLoad();
            }

            @Override
            public void onError(Throwable e) {
                AppLog.e(TAG, "onError: ", e);
            }
        });
    }

    void loadArtist(Album albumParam) {
        AppLog.d(TAG, "loadArtist: ");
        onStartLoad();
        getArtistById.execute(albumParam.getArtistId(), new DisposableSingleObserver<Artist>() {
            @Override
            public void onSuccess(Artist artist) {
                AppLog.d(TAG, "onSuccess: " + artist);
                view.displayArtist(artist);
                onFinishLoad();
            }

            @Override
            public void onError(Throwable e) {
                AppLog.e(TAG, "onError: ", e);
                onFinishLoad();
            }
        });
    }

    private void onFinishLoad() {
        countingIdlingResource.decrement();
    }

    private void onStartLoad() {
        countingIdlingResource.increment();
    }

    private void attachView(AlbumDetailContract.View view) {
        this.view = view;
    }

    @Override
    public void onViewDestroyed() {
        AppLog.d(TAG, "onViewDestroyed: ");
        detachView();
        disposeUseCases();
    }

    private void disposeUseCases() {
        getArtistById.dispose();
        saveAlbum.dispose();
        verifyFavorite.dispose();
        deleteAlbum.dispose();
        getAlbumImage.dispose();
        getAlbumTracks.dispose();
    }

    private void detachView() {
        this.view = null;
    }

    @Override
    public void userWantsToSaveAlbumAsFavorite(Album album, Artist artist, Image albumImage, List<Track> tracks) {
        AppLog.d(TAG, "userWantsToSaveAlbumAsFavorite: " + album);
        saveAlbum(album, artist, albumImage, tracks);
    }

    void saveAlbum(Album album, Artist artist, Image albumImage, List<Track> tracks) {
        onStartLoad();
        SaveAlbumAsFavorite.Request saveRequest = new SaveAlbumAsFavorite.Request(artist, album, albumImage, tracks);
        saveAlbum.execute(saveRequest, new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                AppLog.d(TAG, "onComplete: ");
                view.notifyAlbumSaved();
                onFinishLoad();
            }

            @Override
            public void onError(Throwable e) {
                AppLog.e(TAG, "onError: ", e);
                view.warnErrorWhileSaving();
                onFinishLoad();
            }
        });
    }

    @Override
    public void userWantsToRemoveAlbumAsFavorite(Album album) {
        AppLog.d(TAG, "userWantsToRemoveAlbumAsFavorite: " + album);
        deleteAlbum(album);
    }

    void deleteAlbum(Album album) {
        onStartLoad();
        deleteAlbum.execute(album, new DisposableCompletableObserver() {
            @Override
            public void onComplete() {
                AppLog.d(TAG, "onComplete: ");
                view.notifyAlbumRemoved();
                onFinishLoad();
            }

            @Override
            public void onError(Throwable e) {
                AppLog.e(TAG, "onError: ", e);
                view.warnErrorRemovingAlbum();
                onFinishLoad();
            }
        });
    }

    AlbumDetailContract.View getView() {
        return this.view;
    }
}
