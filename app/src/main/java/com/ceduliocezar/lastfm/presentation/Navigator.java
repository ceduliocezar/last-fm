package com.ceduliocezar.lastfm.presentation;

import android.app.Activity;

import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Artist;


/**
 * Provides navigation through the application screens.
 */
public interface Navigator {

    /**
     * Navigates to the {@link com.ceduliocezar.lastfm.presentation.artist.search.ArtistSearchActivity}
     *
     * @param currentActivity Current activity.
     * @param s
     */
    void navigateToArtistSearch(Activity currentActivity, String s);

    void navigateToArtistTopAlbums(Artist artist, Activity currentActivity);

    void navigateToAlbumDetail(Album album, Activity currentActivity);
}
