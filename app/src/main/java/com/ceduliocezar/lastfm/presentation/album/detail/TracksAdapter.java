package com.ceduliocezar.lastfm.presentation.album.detail;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ceduliocezar.lastfm.R;
import com.ceduliocezar.lastfm.model.entity.Track;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TracksAdapter extends RecyclerView.Adapter<TracksAdapter.ViewHolder> {
    private List<Track> dataSet;
    private OnItemClickListener onItemClickListener;

    public void addAll(List<Track> tracks) {
        dataSet.clear();
        dataSet.addAll(tracks);
        notifyDataSetChanged();
    }

    TracksAdapter(List<Track> dataSet, OnItemClickListener onItemClickListener) {
        this.dataSet = dataSet;
        this.onItemClickListener = onItemClickListener;
    }


    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                         int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.track_item, parent, false);

        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(dataSet.get(position), onItemClickListener);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public interface OnItemClickListener {
        void onItemClick(Track track);
    }

    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.track_name_tv)
        TextView trackNameTextView;

        ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(Track track, OnItemClickListener onItemClickListener) {
            trackNameTextView.setText(track.getName());
            itemView.setOnClickListener(view -> {
                onItemClickListener.onItemClick(track);
            });
        }
    }
}