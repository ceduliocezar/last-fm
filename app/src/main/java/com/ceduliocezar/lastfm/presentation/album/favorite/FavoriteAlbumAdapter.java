package com.ceduliocezar.lastfm.presentation.album.favorite;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ceduliocezar.lastfm.R;
import com.ceduliocezar.lastfm.model.entity.Album;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class FavoriteAlbumAdapter extends RecyclerView.Adapter<FavoriteAlbumAdapter.AlbumViewHolder> {
    private List<Album> dataSet;
    private com.ceduliocezar.lastfm.presentation.album.top.AlbumAdapter.OnItemClickListener onItemClickListener;

    public void addAll(List<Album> Albums) {
        dataSet.clear();
        dataSet.addAll(Albums);
        notifyDataSetChanged();
    }

    FavoriteAlbumAdapter(List<Album> dataSet, com.ceduliocezar.lastfm.presentation.album.top.AlbumAdapter.OnItemClickListener onItemClickListener) {
        this.dataSet = dataSet;
        this.onItemClickListener = onItemClickListener;
    }


    @NonNull
    @Override
    public FavoriteAlbumAdapter.AlbumViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                                   int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.album_item, parent, false);

        return new FavoriteAlbumAdapter.AlbumViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull FavoriteAlbumAdapter.AlbumViewHolder holder, int position) {
        holder.bind(dataSet.get(position), onItemClickListener);
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public interface OnItemClickListener {
        void onItemClick(Album Album);
    }

    static class AlbumViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.album_name_tv)
        TextView nameTextView;

        AlbumViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(Album Album, com.ceduliocezar.lastfm.presentation.album.top.AlbumAdapter.OnItemClickListener onItemClickListener) {
            nameTextView.setText(Album.getName());
            itemView.setOnClickListener(view -> {
                onItemClickListener.onItemClick(Album);
            });
        }
    }
}