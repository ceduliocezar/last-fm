package com.ceduliocezar.lastfm.presentation.artist.search;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.ceduliocezar.lastfm.R;
import com.ceduliocezar.lastfm.model.entity.Artist;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ArtistAdapter extends RecyclerView.Adapter<ArtistAdapter.MyViewHolder> {
    private List<Artist> dataSet;
    private OnItemClickListener onItemClickListener;

    public void addAll(List<Artist> artists) {
        dataSet.clear();
        dataSet.addAll(artists);
        notifyDataSetChanged();
    }


    ArtistAdapter(List<Artist> dataSet, OnItemClickListener onItemClickListener) {
        this.dataSet = dataSet;
        this.onItemClickListener = onItemClickListener;
    }


    @NonNull
    @Override
    public ArtistAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent,
                                                         int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.artist_item, parent, false);

        return new MyViewHolder(v);
    }


    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.bind(dataSet.get(position), onItemClickListener);

    }


    @Override
    public int getItemCount() {
        return dataSet.size();
    }

    public interface OnItemClickListener {
        void onItemClick(Artist artist);
    }

    static class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.artist_name_tv)
        TextView nameTextView;

        MyViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(Artist artist, OnItemClickListener onItemClickListener) {
            nameTextView.setText(artist.getName());
            itemView.setOnClickListener(view -> {
                onItemClickListener.onItemClick(artist);
            });
        }
    }
}