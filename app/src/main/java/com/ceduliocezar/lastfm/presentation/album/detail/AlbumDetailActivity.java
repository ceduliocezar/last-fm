package com.ceduliocezar.lastfm.presentation.album.detail;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.test.espresso.idling.CountingIdlingResource;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.ceduliocezar.lastfm.CustomApplication;
import com.ceduliocezar.lastfm.R;
import com.ceduliocezar.lastfm.logging.AppLog;
import com.ceduliocezar.lastfm.model.entity.Album;
import com.ceduliocezar.lastfm.model.entity.Artist;
import com.ceduliocezar.lastfm.model.entity.Image;
import com.ceduliocezar.lastfm.model.entity.Track;
import com.varunest.sparkbutton.SparkButton;
import com.varunest.sparkbutton.SparkEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AlbumDetailActivity extends AppCompatActivity implements AlbumDetailContract.View, TracksAdapter.OnItemClickListener, SparkEventListener {

    private static final String TAG = "AlbumDetailActivity";

    public static final String EXTRA_ALBUM = "EXTRA_ALBUM";

    @BindView(R.id.artist_tv)
    TextView artistNameTextView;
    @BindView(R.id.album_tv)
    TextView albumTextView;

    @BindView(R.id.container_view)
    View containerView;

    @BindView(R.id.album_iv)
    ImageView imageView;

    @BindView(R.id.tracks_recycler)
    RecyclerView tracksRecycler;

    @BindView(R.id.favorite_button)
    SparkButton sparkButton;

    @Inject
    AlbumDetailContract.Presenter presenter;
    @Inject
    CountingIdlingResource countingIdlingResource;

    private Image albumImage;
    private Artist artist;
    private List<Track> tracks;
    private TracksAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_album_detail);
        ((CustomApplication) getApplication()).component().inject(this);
        ButterKnife.bind(this);
        setupRecycler();
        setupSpark();

        presenter.onViewCreated(this);
    }

    private void setupSpark() {
        sparkButton.setEventListener(this);
    }

    private void setupRecycler() {
        adapter = new TracksAdapter(new ArrayList<>(), this);
        tracksRecycler.setAdapter(adapter);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onViewDestroyed();
    }


    @Override
    public void displayArtist(Artist artist) {
        AppLog.d(TAG, "displayArtist: " + artist);
        this.artist = artist;
        artistNameTextView.setText(artist.getName());
    }

    @Override
    public void displayTracks(List<Track> trackList) {
        AppLog.d(TAG, "displayTracks: " + trackList);
        this.tracks = trackList;
        this.adapter.addAll(trackList);
    }

    @Override
    public void displayAlbum(Album album) {
        AppLog.d(TAG, "displayAlbum: " + album);
        albumTextView.setText(album.getName());
    }

    @Override
    public Album getAlbumParam() {
        return (Album) Objects.requireNonNull(getIntent().getExtras()).getSerializable(EXTRA_ALBUM);
    }

    @Override
    public void notifyAlbumSaved() {
        AppLog.d(TAG, "notifyAlbumSaved: ");
        Snackbar.make(containerView, R.string.album_saved_as_favorite,
                Snackbar.LENGTH_SHORT)
                .show();
    }

    @Override
    public void warnErrorWhileSaving() {
        AppLog.d(TAG, "warnErrorWhileSaving: ");
        Snackbar.make(containerView, R.string.warn_error_while_saving_album,
                Snackbar.LENGTH_SHORT)
                .show();
    }

    @Override
    public void notifyAlbumRemoved() {
        AppLog.d(TAG, "notifyAlbumRemoved: ");
        Snackbar.make(containerView, R.string.album_removed_from_favorite,
                Snackbar.LENGTH_SHORT)
                .show();
    }

    @Override
    public void warnErrorRemovingAlbum() {
        AppLog.d(TAG, "warnErrorRemovingAlbum: ");
        Snackbar.make(containerView, R.string.warn_error_removing_album_favorites,
                Snackbar.LENGTH_SHORT)
                .show();
    }

    @Override
    public void displayImage(Image image) {
        AppLog.d(TAG, "displayImage: " + image);
        this.albumImage = image;
        Glide.with(this)
                .load(image.getUrl())
                .apply(RequestOptions.diskCacheStrategyOf(DiskCacheStrategy.NONE).placeholder(R.drawable.ic_album_place_holder))
                .into(imageView);
    }

    @Override
    public void displayFavorite(Boolean favorite) {
        AppLog.d(TAG, "displayFavorite: " + favorite);
        sparkButton.setEventListener(null);
        sparkButton.setChecked(favorite);
        sparkButton.setEventListener(this);
    }

    @Override
    public void onItemClick(Track track) {
        AppLog.d(TAG, "onItemClick: " + track);
        // TODO: 11/09/18 open online music player?
    }

    @Override
    public void onEvent(ImageView button, boolean buttonState) {
        if (buttonState) {
            presenter.userWantsToSaveAlbumAsFavorite(getAlbumParam(), artist, albumImage, tracks);
        } else {
            presenter.userWantsToRemoveAlbumAsFavorite(getAlbumParam());
        }
    }

    @Override
    public void onEventAnimationEnd(ImageView button, boolean buttonState) {
        // nothing to do
    }

    @Override
    public void onEventAnimationStart(ImageView button, boolean buttonState) {
        // nothing to do
    }
}
