package com.ceduliocezar.lastfm;

import android.app.Application;
import android.support.test.espresso.idling.CountingIdlingResource;

import com.ceduliocezar.lastfm.di.AppComponent;
import com.ceduliocezar.lastfm.di.DaggerAppComponent;
import com.ceduliocezar.lastfm.di.module.AppModule;

import javax.inject.Inject;

public class CustomApplication extends Application {

    private AppComponent component = createComponent();

    @Inject
    public CountingIdlingResource countingIdlingResource;

    @Override
    public void onCreate() {
        super.onCreate();

        component.inject(this);
    }

    public AppComponent createComponent() {

        return DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public AppComponent component() {
        return component;
    }
}
