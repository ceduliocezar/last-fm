package com.ceduliocezar.lastfm.logging;

import android.util.Log;

/**
 * Encapsulates logging implementation.
 */
public class AppLog {

    public static void d(String tag, String message) {
        Log.d(tag, message);
    }

    public static void e(String tag, String s, Throwable e) {
        Log.e(tag, s, e);
    }

    public static void i(String tag, String s) {
        Log.i(tag, s);
    }
}
